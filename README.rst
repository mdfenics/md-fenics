MD-FEniCS

Description
-----------

MD-FEniCS is a high-level/high-performance Open Source Multiscale Coupling Molecular Dynamics with Finite Element based on FEniCS.

Authors
-------

MD-FEniCS is developed by
  * Michele Cascio 
  * Antonio La Magna

Contributors:
  * Davide Baroli 

Licence
-------

MD-FEniCS is licensed under the GNU GPL, version 3 or (at your option) any
later version.

MD-FEniCS is Copyright (2016-2019) by the authors.
