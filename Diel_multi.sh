#!/bin/bash
# Sets the core number using the first argument of the script
CORE_NUMB=$1
NCYCLE_NUMB=$2
echo "Diel run number of cores $CORE_NUMB"
#echo "Diel run number of cycles $NCYCLE_NUMB"
python mesh_gen.py &&\
mpirun -n $CORE_NUMB python init_diel.py
for i in {1..20000..1}
  do
     python mesh_gen.py &&\
     mpirun -n $CORE_NUMB python cycle_diel.py
  done
echo "Last Cycle done"
