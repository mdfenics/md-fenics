from dolfin import *
from mshr import  *
import sys
import os
import string
import numpy
import xml.etree.ElementTree
#import pygmsh as pg
import numpy as np
#import stl
#from stl import mesh
import subprocess
import shutil
import time

#option=1:  out-of-surface integration and project utilization
#option=2:  surface integration and use of projects
#option=3:  out-of-surface integration and use of grad(u)
#option=4:  integration on the surface and use of grad(u)


time_in=time.time()

comm = mpi_comm_world()
rank = MPI.rank(comm)
size_mpi= MPI.size(comm)


if rank == 0 :
    print "\nSTART init_diel.py\n"
else:
    pass

path="./InputFiles"
os.chdir(path)
tree_J = xml.etree.ElementTree.parse('Job.xml')
root_J = tree_J.getroot()
solver_name=root_J[0].attrib['Value']
geometry_name=root_J[1].attrib['Value']
particle_name=root_J[2].attrib['Value']

# Solver and integration options
tree_S = xml.etree.ElementTree.parse(solver_name)
root_S = tree_S.getroot()


delta_t=float(root_S[0].attrib['Value']) #2.e-5
t_poisson_factor=int(root_S[1].attrib['Value'])
t_poisson = t_poisson_factor*delta_t
linear_solver = root_S[2].attrib['Value']
abs_tol=float(root_S[3].attrib['Value'])
rel_tol=float(root_S[4].attrib['Value'])
div_lim=float(root_S[5].attrib['Value'])
if (root_S[6].attrib['Value'] == 'false') :
   no0_ini= False
else :
   no0_ini= True
if (root_S[7].attrib['Value'] == 'false') :
   verb_log = False
else :
   verb_log = True
option=float(root_S[8].attrib['Value'])

# Geometry options
tree_G = xml.etree.ElementTree.parse(geometry_name)
root_G = tree_G.getroot()
N_electrodes=int(root_G[0].attrib['Value'])  #2
electrodes_width=float(root_G[1].attrib['Value']) #40e-6
L_box=2*N_electrodes*electrodes_width
h_box=float(root_G[2].attrib['Value']) #100.e-6
depth_box=float(root_G[3].attrib['Value']) #60e-6
L_BC_Neumann=float(root_G[4].attrib['Value']) #40.e-6
L_el_Power=float(root_G[5].attrib['Value']) #40.e-6
Len_car_box=float(root_G[6].attrib['Value']) #4.e-6
rad=float(root_G[7].attrib['Value']) #6.e-6  #raggio

tree_P = xml.etree.ElementTree.parse(particle_name)
root_P = tree_P.getroot()
# Read data for Initial Particle Position (Center)


X0 = []
v_x_0 = []
v_y_0 = []
v_z_0 = []
for particle in root_P:
    X0.append([float(particle.attrib['XC']),float(particle.attrib['YC']),float(particle.attrib['ZC'])])
    v_x_0.append(float(particle.attrib['VX']))
    v_y_0.append(float(particle.attrib['VY']))
    v_z_0.append(float(particle.attrib['VZ']))
# rescale time scale pulse
N_particles=len(X0)
path=".."
os.chdir(path)


#RADII
R=[]
if option==1 or option==3:
  i=0
  while i<=N_particles-1:
      R.append(rad+0.1e-6)
      i+=1
else:
    i=0
    while i<=N_particles-1:
        R.append(rad)
        i+=1


#QUANTITIES FOR THE CALCULATION OF FORCES
mu=0.00131  #dynamic viscosity
m=1.0527e-12   # cell mass
u_fluido_max=100e-6
gamma= 6*3.141*rad*mu
tau= m/gamma   # F = m*a= -gamma*v_rel  =>  a= - gamma*v_rel/m = -v_rel/tau => tau=m/gamma
C=0.172 # lift force factor
acc_g = 9.81
coef=  1./(1.+0.5*(delta_t/tau))  # for the verlet method

# GENERAL ELECTROMAGNETIC QUANTITIES
eps_0 = 8.854187818e-12
V_rms=5

frequency=1e6
omega= 2*3.141*frequency

medium_density=1000
eps_fluid_r = 79*eps_0
sigma_fluid=0.03 # con 0.01 viene Refcm=0,85 alla frequenza 1 MHz
eps_fluid_imm=-sigma_fluid/omega

## ELECTROMAGNETIC QUANTITIES MDA-231. Calculation of EPS_EFF
particle_density=1077
eps_mem_r=24*eps_0
sigma_mem=1e-7
eps_mem_imm=-sigma_mem/omega
#print ("eps_mem_imm=",eps_mem_imm)

eps_cyt_r=50*eps_0
sigma_cyt=0.2
eps_cyt_imm=-sigma_cyt/omega

# calculation eps_eff:
#eps_eff = eps_mem*((gamma_cube +2A)/(gamma_cube-A))=eps_mem*rapporto
gamma_cube=pow((rad/(rad-1e-8)),3)
a=eps_cyt_r-eps_mem_r
b=eps_cyt_imm-eps_mem_imm
c=eps_cyt_r+2*eps_mem_r
d=eps_cyt_imm+2*eps_mem_imm
A_r=(a*c+b*d)/(c*c+d*d)
A_i=(b*c-a*d)/(c*c+d*d)

ratio_real=((gamma_cube+2.*A_r)*(gamma_cube-A_r)-2.*A_i*A_i)/(pow((gamma_cube-A_r),2)+A_i*A_i)
ratio_imm=(A_i*3*gamma_cube)/(pow((gamma_cube-A_r),2)+A_i*A_i)
eps_eff_r = eps_mem_r*ratio_real - eps_mem_imm*ratio_imm
eps_eff_imm = eps_mem_imm*ratio_real + eps_mem_r*ratio_imm

#print "eps eff_r in unita di eps0", eps_eff_r/eps_0
#print "eps eff imm in unita di eps0",eps_eff_imm/eps_0

##  F_CM
a=eps_eff_r-eps_fluid_r
b=eps_eff_imm-eps_fluid_imm
c=eps_eff_r+2*eps_fluid_r
d=eps_eff_imm+2*eps_fluid_imm
Re_fcm=(a*c+b*d)/(c*c+d*d)
#print "\nRe_f_CM MDA-231=", Re_fcm


# Subdomain for Periodic boundary condition
class PeriodicBoundary(SubDomain):
    # Left boundary is "target domain" G
    def inside(self, x, on_boundary):
        return bool(x[0] < DOLFIN_EPS and x[0] > -DOLFIN_EPS and on_boundary)
    # Map right boundary (H) to left boundary (G)
    def map(self, x, y):
        y[0] = x[0] - L_box
        y[1] = x[1]
        y[2] = x[2]


class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return (near(x[2], 0) and between(x[0], (x_sx, x_dx)))


# FILE DI OUTPUT pvd

outfile_header="output"
file_u = File(outfile_header+"_soluzione_poisson.pvd")
# FILE DI OUTPUT
if rank == 0:
    out_VOL_SPHERE=open(outfile_header+"VOL_SPHERE.csv","w")
    out_SURFACE_SPHERE=open(outfile_header+"SURFACE_SPHERE.csv","w")
    #out_INITIAL_VALUES=open(outfile_header+"INITIAL_VALUES.csv","w")
    out_CENTRE_x=open(outfile_header+"CENTRE_x.csv","w")
    out_CENTRE_y=open(outfile_header+"CENTRE_y.csv","w")
    out_CENTRE_z=open(outfile_header+"CENTRE_z.csv","w")
    #out_CENTRE_x_LOOP=open(outfile_header+"CENTRE_x_LOOP.csv","w")
    #out_CENTRE_y_LOOP=open(outfile_header+"CENTRE_y_LOOP.csv","w")
    #out_CENTRE_z_LOOP=open(outfile_header+"CENTRE_z_LOOP.csv","w")
    #out_CENTRE_x_old=open(outfile_header+"CENTRE_x_old.csv","w")
    #out_CENTRE_y_old=open(outfile_header+"CENTRE_y_old.csv","w")
    #out_CENTRE_z_old=open(outfile_header+"CENTRE_z_old.csv","w")
    out_ACCEL_MST_x_1=open(outfile_header+"A_MST_x_1.csv","w")
    out_ACCEL_MST_y_1=open(outfile_header+"A_MST_y_1.csv","w")
    out_ACCEL_MST_z_1=open(outfile_header+"A_MST_z_1.csv","w")
    out_A_VISC_x_1=open(outfile_header+"A_VISC_x_1.csv","w")
    out_A_VISC_y_1=open(outfile_header+"A_VISC_y_1.csv","w")
    out_A_VISC_z_1=open(outfile_header+"A_VISC_z_1.csv","w")
    out_A_LIFT_y_1=open(outfile_header+"A_LIFT_y_1.csv","w")
    out_A_LIFT_z_1=open(outfile_header+"A_LIFT_z_1.csv","w")
    out_ACCEL_x_1=open(outfile_header+"ACCEL_x.csv","w")
    out_ACCEL_y_1=open(outfile_header+"ACCEL_y.csv","w")
    out_ACCEL_z_1=open(outfile_header+"ACCEL_z.csv","w")
    #out_VEL_z_0=open(outfile_header+"VEL_z_0.csv","w")
    out_VEL_x_1=open(outfile_header+"VEL_x_1.csv","w")
    out_VEL_y_1=open(outfile_header+"VEL_y_1.csv","w")
    out_VEL_z_1=open(outfile_header+"VEL_z_1.csv","w")
    #out_VEL_RELATIVA_x_1=open(outfile_header+"U_x_1.csv","w")
    out_LOOP=open(outfile_header+"LOOP.csv","w")
else :
     pass
# DEFINIZIONI ARRAY

centrex=[]
centrey=[]
centrez=[]
centre_x_old=[]
centre_y_old=[]
centre_z_old=[]
vol_sphere=[]
surface_sphere=[]
#v_x_0=[]
v_x_1=[]
u_x_0=[]
u_x_1=[]
v_rel_x_0 =[]
v_rel_x_1 =[]
#v_y_0=[]
v_y_1=[]
#v_z_0=[]
v_z_1=[]
cos_x=[]
cos_y=[]
cos_z=[]
F_MST_x_0=[]
F_MST_x_1=[]
F_MST_y_0=[]
F_MST_y_1=[]
F_MST_z_0=[]
F_MST_z_1=[]

F_lift_y_0=[]
F_lift_y_1=[]

F_lift_z_0=[]
F_lift_z_1=[]

a_visc_x_0=[]
a_visc_x_1=[]

a_visc_y_0=[]
a_visc_y_1=[]

a_visc_z_0=[]
a_visc_z_1=[]

a_lift_y_0=[]
a_lift_y_1=[]

a_lift_z_0=[]
a_lift_z_1=[]

a_MST_x_0=[]
a_MST_x_1=[]

a_MST_y_0=[]
a_MST_y_1=[]

a_MST_z_0=[]
a_MST_z_1=[]

a_x_0=[]
a_x_1=[]

a_y_0=[]
a_y_1=[]

a_z_0=[]
a_z_1=[]

#INIZIALIZZAZIONE ARRAY
i=1
while i<=N_particles:

  vol_sphere.append(0.)
  surface_sphere.append(0.)

  centrex.append(0.)
  centrey.append(0.)
  centrez.append(0.)

  centre_x_old.append(0.)
  centre_y_old.append(0.)
  centre_z_old.append(0.)

  #v_x_0.append(0)
  v_x_1.append(0)

  u_x_0.append(0.)
  u_x_1.append(0.)

  v_rel_x_0.append(0.)
  v_rel_x_1.append(0.)

  #v_y_0.append(0.)
  v_y_1.append(0)

  #v_z_0.append(0.)
  v_z_1.append(0.)

  cos_x.append(0.)
  cos_y.append(0.)
  cos_z.append(0.)

  F_MST_x_0.append(0.)
  F_MST_y_0.append(0.)
  F_MST_z_0.append(0.)

  F_MST_x_1.append(0.)
  F_MST_y_1.append(0.)
  F_MST_z_1.append(0.)

  F_lift_y_0.append(0.)
  F_lift_y_1.append(0.)

  F_lift_z_0.append(0.)
  F_lift_z_1.append(0.)

  a_lift_y_0.append(0.)
  a_lift_y_1.append(0.)

  a_lift_z_0.append(0.)
  a_lift_z_1.append(0.)

  a_x_1.append(0.)
  a_y_1.append(0.)
  a_z_1.append(0.)

  a_MST_x_0.append(0.)
  a_MST_y_0.append(0.)
  a_MST_z_0.append(0.)

  a_MST_x_1.append(0.)
  a_MST_y_1.append(0.)
  a_MST_z_1.append(0.)

  a_visc_x_0.append(0.)
  a_visc_y_0.append(0.)
  a_visc_z_0.append(0.)

  a_visc_x_1.append(0.)
  a_visc_y_1.append(0.)
  a_visc_z_1.append(0.)

  a_x_0.append(0.)
  a_y_0.append(0.)
  a_z_0.append(0.)

  i+=1


MPI.barrier(comm)

parameters["ghost_mode"] = "shared_vertex"
mesh = Mesh()
hdf = HDF5File(mesh.mpi_comm(), "file.h5", "r")
hdf.read(mesh, "/mesh", False)
mv = MeshFunction("size_t", mesh, mesh.topology().dim()) #CellFunction
hdf.read(mv, "/mv")
mf = MeshFunction("size_t", mesh, mesh.topology().dim()-1) #FacetFunction
hdf.read(mf, "/mf")
hdf.close()
dx = Measure("dx", domain=mesh, subdomain_data=mv)
ds = Measure("ds", domain=mesh, subdomain_data=mf)
dS = Measure("dS", domain=mesh, subdomain_data=mf)

#vol_sph_t0=[]
#sup_sph_t0=[]

#i=0
#while i<=N_particles-1:
#    vol_sph_t0.append(0)
#    vol_sph_t0[i] = assemble(1.*dx(i+1))
#    sup_sph_t0.append(0)
#    sup_sph_t0[i] = assemble(1.*dS(i+1))
#    i=i+1

vol_box =assemble(1.*dx(N_particles+1))
sur_dx=assemble(1.*ds(N_particles+1))
sur_sx =assemble(1.*ds(N_particles+6))
sur_top =assemble(1.*ds(N_particles+3))
sur_bottom =assemble(1.*ds(N_particles+4))
sur_rear_face =assemble(1.*ds(N_particles+2))
sur_front_face=assemble(1.*ds(N_particles+5))

#if rank == 0 :
   #print vol_box,vol_box,sur_sx,sur_top,sur_bottom,sur_rear_face,sur_front_face
#else :
    #pass


i=0
while i<=N_particles-1:
    centrex[i]=X0[i][0]
    centrey[i]=X0[i][1]
    centrez[i]=X0[i][2]

    i=i+1

#V = FunctionSpace(mesh, "CG", 1,constrained_domain=PeriodicBoundary())    #era 2

degree_L=2

Pot_r = FiniteElement("CG", mesh.ufl_cell(), degree_L-1)
Pot_i = FiniteElement("CG", mesh.ufl_cell(), degree_L-1)

Pot_c = Pot_r * Pot_i

W = FunctionSpace(mesh,Pot_c)
#W = FunctionSpace(mesh,Pot_c,constrained_domain=PeriodicBoundary())

bcs = [DirichletBC(W.sub(0), 0.0, mf, N_particles+7), DirichletBC(W.sub(0), V_rms, mf, N_particles+2), DirichletBC(W.sub(0), 0.0, mf, N_particles+1),DirichletBC(W.sub(1), 0.0, mf, N_particles+7),DirichletBC(W.sub(1), 0.0, mf, N_particles+2),DirichletBC(W.sub(1), 0.0, mf, N_particles+1)]


P = TrialFunction(W)
P_r, P_i = split(P)

(v_r, v_i) = TestFunctions(W)

f_r = Constant(0.0)
f_i = Constant(0.0)

if option==1 or option==3:
    V1=FiniteElement("CG", mesh.ufl_cell(), degree_L-1)
    V=FunctionSpace(mesh,V1)
#    V=FunctionSpace(mesh,V1,constrained_domain=PeriodicBoundary())
    dofmap = V.dofmap()
    my_first, my_last = dofmap.ownership_range() # global
    coord_mesh = V.tabulate_dof_coordinates().reshape((-1, 3))
    unowned = dofmap.local_to_global_unowned()
    dofs = filter(lambda dof: dofmap.local_to_global_index(dof) not in unowned, xrange(my_last-my_first))
    coord_mesh=coord_mesh[dofs]

    #eps_eff_r=eps_fluid_r
    #eps_eff_imm=eps_fluid_imm

    # REAL PART OF EPSILON

    eps_r = Function(V)
    eps_r = interpolate(Constant(eps_fluid_r), V)
    eps_r_array = eps_r.vector().get_local()
    i=0
    while i<=N_particles-1:
     for dof in dofs:
          point=coord_mesh[dof]
          distance = pow(pow(X0[i][0]-point[0],2)+pow(X0[i][1]-point[1],2)+pow(X0[i][2]-point[2],2),0.5)
          if (distance < rad):
            eps_r_array[dof] = eps_eff_r
     i+=1
    eps_r.vector().set_local(eps_r_array)
    # Immaginary part of eps
    eps_imm = Function(V)
    eps_imm = interpolate(Constant(eps_fluid_imm), V)
    eps_imm_array = eps_r.vector().get_local()
    i=0
    while i<=N_particles-1:
        for dof in dofs :
            point=coord_mesh[dof]
            distance = pow(pow(X0[i][0]-point[0],2)+pow(X0[i][1]-point[1],2)+pow(X0[i][2]-point[2],2),0.5)
            if (distance < rad):
                eps_imm_array[dof] = eps_eff_imm
        i+=1
    eps_imm.vector().set_local(eps_imm_array)
    a_r = inner(eps_r*grad(P_r),grad(v_r))*dx - inner(eps_imm*grad(P_i),grad(v_r))*dx
    a_i = inner(eps_imm*grad(P_r),grad(v_i))*dx + inner(eps_r*grad(P_i),grad(v_i))*dx
    L_r=f_r*v_r*dx- f_i*v_i*dx
    L_i=f_r*v_i*dx+ f_i*v_r*dx
    a=a_r + a_i
    L=L_r + L_i
    u = Function(W)
    problem = LinearVariationalProblem(a, L, u, bcs)
    solver = LinearVariationalSolver(problem)
    # Set linear solver parameters
    prm = solver.parameters
    if linear_solver == 'Krylov':
       prm.linear_solver = 'gmres'
       if size_MPI == 1: # serial
          prm.preconditioner = 'ilu'
       else :            # parellel
            prm.preconditioner = 'hypre_euclid'
       prm.krylov_solver.absolute_tolerance = abs_tol
       prm.krylov_solver.relative_tolerance = rel_tol
       prm.krylov_solver.divergence_limit = div_lim
       prm.krylov_solver.nonzero_initial_guess=no0_ini
    else:
         prm.linear_solver = 'lu'
    solver.solve()
#    a = assemble(a_r + a_i)
#    L = assemble(L_r + L_i)
#    for bc in bcs: bc.apply(a,L)
#    u = Function(W)
#    solve (a, u.vector(), L)
    u_r,u_i = split(u)
    file = File("Solution_t0.pvd")
    file << u
    #file = File("\neps_imm_t0.pvd")
    #file << eps_imm
    #file = File("eps_r_t0.pvd")
    #file << eps_r
    #if rank == 0 :
        #print "\neps_fluid_r=", eps_fluid_r
        #print "eps_fluid_i=", eps_fluid_imm
        #print "\neps_eff_r=", eps_eff_r
        #print "eps_eff_i=", eps_eff_imm
        
    #else :
         #pass
else:
    a_r = inner(eps_fluid_r*grad(P_r),grad(v_r))*dx(N_particles+1) - inner(eps_fluid_imm*grad(P_i),grad(v_r))*dx(N_particles+1)
    a_i = inner(eps_fluid_imm*grad(P_r),grad(v_i))*dx(N_particles+1) + inner(eps_fluid_r*grad(P_i),grad(v_i))*dx(N_particles+1)
    L_r=f_r*v_r*dx(N_particles+1)- f_i*v_i*dx(N_particles+1)
    L_i=f_r*v_i*dx(N_particles+1)+ f_i*v_r*dx(N_particles+1)
    #eps_eff_r=eps_fluid_r
    #eps_eff_imm=eps_fluid_imm
    i=1
    while i<=N_particles:
        a_r = a_r + inner(eps_eff_r*grad(P_r),grad(v_r))*dx(i) - inner(eps_eff_imm*grad(P_i),grad(v_r))*dx(i)
        a_i = a_i + inner(eps_eff_imm*grad(P_r),grad(v_i))*dx(i) + inner(eps_eff_r*grad(P_i),grad(v_i))*dx(i)
        #L_r = L_r + inner(f_r,v_r)*dx(i) - inner(f_i,v_i)*dx(i)
        #L_i = L_i + inner(f_r,v_i)*dx(i) + inner(f_i,v_r)*dx(i)
        L_r = L_r + f_r*v_r*dx(i)- f_i*v_i*dx(i)
        L_i = L_i + f_r*v_i*dx(i)+ f_i*v_r*dx(i)
        i+=1
    a=a_r + a_i
    L=L_r + L_i
    u = Function(W)
    problem = LinearVariationalProblem(a, L, u, bcs)
    solver = LinearVariationalSolver(problem)
    # Set linear solver parameters
    prm = solver.parameters
    if linear_solver == 'Krylov':
       prm.linear_solver = 'gmres'
       if size_MPI == 1: # serial
          prm.preconditioner = 'ilu'
       else :            # parellel
            prm.preconditioner = 'hypre_euclid'
       prm.krylov_solver.absolute_tolerance = abs_tol
       prm.krylov_solver.relative_tolerance = rel_tol
       prm.krylov_solver.divergence_limit = div_lim
       prm.krylov_solver.nonzero_initial_guess=no0_ini
    else:
         prm.linear_solver = 'lu'
    solver.solve()
  #  a = assemble(a_r + a_i)
  #  L = assemble(L_r + L_i)
  #  for bc in bcs: bc.apply(a,L)
  #  u = Function(W)
  #  solve (a, u.vector(), L)
    u_r,u_i = split(u)
    file = File("Solution_t0.pvd")
    file << u
p10 = Point(40.e-6,30.e-6,10.e-6)
p15= Point(40.e-6,30.e-6,15.e-6)
p20 = Point(40.e-6,30.e-6,20.e-6)
p25 = Point(40.e-6,30.e-6,25.e-6)
p30 = Point(40.e-6,30.e-6,30.e-6)
p35 = Point(40.e-6,30.e-6,35.e-6)
p40 = Point(40.e-6,30.e-6,40.e-6)
p45 = Point(40.e-6,30.e-6,45.e-6)
p50 = Point(40.e-6,30.e-6,50.e-6)
p60 = Point(40.e-6,30.e-6,60.e-6)
p70 = Point(40.e-6,30.e-6,70.e-6)
p80 = Point(40.e-6,30.e-6,80.e-6)
p90 = Point(40.e-6,30.e-6,90.e-6)
#print "V1=", u_r(p10)
#print "V2=", u_r(p20)
#print "V3=", u_r(p30)
#print "V4=", u_r(p40)
#print "V5=", u_r(p50)
#print "V6=", u_r(p60)
#print "V7=", u_r(p70)
#print "V8=", u_r(p80)
#print "V9=", u_r(p90)
if option==1 or option==2:
    if rank == 0 :
       time_start_project=time.clock()
       print "\ntime_start_project=", time_start_project
    else :
         pass
    VFS = VectorFunctionSpace(mesh, 'CG', 1)
    E_r = project(as_vector(-nabla_grad(u_r)), VFS)
    E_i = project(as_vector(-nabla_grad(u_i)), VFS)
    file = File("E_real_t0.pvd")
    file << E_r
    file = File("E_imm_t0.pvd")
    file << E_i
    if rank == 0 :
       time_end_project=time.clock()
       print "time_end_project=", time_end_project
       time_project=time_end_project-time_start_project
       print "time_project=", time_project
    else :
         pass
    i=0
    while i<=N_particles-1:
    
        cos_xi=Expression("(x[0]-x_c)/pow((x[0]-x_c)*(x[0]-x_c)+(x[1]-y_c)*(x[1]-y_c)+(x[2]-z_c)*(x[2]-z_c),0.5)", x_c=centrex[i], y_c=centrey[i], z_c=centrez[i] ,degree=2)
        cos_yi=Expression("(x[1]-y_c)/pow((x[0]-x_c)*(x[0]-x_c)+(x[1]-y_c)*(x[1]-y_c)+(x[2]-z_c)*(x[2]-z_c),0.5)", x_c=centrex[i], y_c=centrey[i], z_c=centrez[i] ,degree=2)
        cos_zi=Expression("(x[2]-z_c)/pow((x[0]-x_c)*(x[0]-x_c)+(x[1]-y_c)*(x[1]-y_c)+(x[2]-z_c)*(x[2]-z_c),0.5)", x_c=centrex[i], y_c=centrey[i], z_c=centrez[i] ,degree=2)
        
        # F_MST_x 
        T_xx=E_i[0]*E_i[0]+E_r[0]*E_r[0]-E_i[1]*E_i[1]-E_r[1]*E_r[1]-E_i[2]*E_i[2]-E_r[2]*E_r[2]
        T_xy=E_r[0]*E_r[1]+E_r[0]*E_r[1]+E_i[0]*E_i[1]+E_i[0]*E_i[1]
        T_xz=E_r[0]*E_r[2]+E_r[0]*E_r[2]+E_i[0]*E_i[2]+E_i[0]*E_i[2]
        F_MST_x_0[i]=(eps_fluid_r/4.)*(assemble((T_xx*cos_xi+ T_xy*cos_yi+T_xz*cos_zi)*dS(i+1)))
        a_MST_x_0[i]=F_MST_x_0[i]/m
        
        # F_MST_y  
        T_yx=E_r[1]*E_r[0]+E_r[1]*E_r[0]+E_i[1]*E_i[0]+E_i[1]*E_i[0]
        T_yy=E_i[1]*E_i[1]+E_r[1]*E_r[1]-E_i[0]*E_i[0]-E_r[0]*E_r[0]-E_i[2]*E_i[2]-E_r[2]*E_r[2]
        T_yz=E_r[1]*E_r[2]+E_r[1]*E_r[2]+E_i[1]*E_i[2]+E_i[1]*E_i[2]
        F_MST_y_0[i]=(eps_fluid_r/4.0)*(assemble((T_yx*cos_xi+ T_yy*cos_yi+T_yz*cos_zi) * dS(i+1)))
        a_MST_y_0[i]=F_MST_y_0[i]/m
        
        # F_MST_z  
        T_zx=E_r[2]*E_r[0]+E_r[2]*E_r[0]+E_i[2]*E_i[0]+E_i[2]*E_i[0]
        T_zy=E_r[2]*E_r[1]+E_r[2]*E_r[1]+E_i[2]*E_i[1]+E_i[2]*E_i[1]
        T_zz=E_i[2]*E_i[2]+E_r[2]*E_r[2]-E_i[0]*E_i[0]-E_r[0]*E_r[0]-E_i[1]*E_i[1]-E_r[1]*E_r[1]
        F_MST_z_0[i]=(eps_fluid_r/4.0)*(assemble((T_zx*cos_xi+ T_zy*cos_yi+T_zz*cos_zi) * dS(i+1)))
        a_MST_z_0[i]=F_MST_z_0[i]/m
        i=i+1
else:
  i=0
  while i<=N_particles-1:
    
    cos_xi=Expression("(x[0]-x_c)/pow((x[0]-x_c)*(x[0]-x_c)+(x[1]-y_c)*(x[1]-y_c)+(x[2]-z_c)*(x[2]-z_c),0.5)", x_c=centrex[i], y_c=centrey[i], z_c=centrez[i] ,degree=2)
    cos_yi=Expression("(x[1]-y_c)/pow((x[0]-x_c)*(x[0]-x_c)+(x[1]-y_c)*(x[1]-y_c)+(x[2]-z_c)*(x[2]-z_c),0.5)", x_c=centrex[i], y_c=centrey[i], z_c=centrez[i] ,degree=2)
    cos_zi=Expression("(x[2]-z_c)/pow((x[0]-x_c)*(x[0]-x_c)+(x[1]-y_c)*(x[1]-y_c)+(x[2]-z_c)*(x[2]-z_c),0.5)", x_c=centrex[i], y_c=centrey[i], z_c=centrez[i] ,degree=2)
    # F_MST_x e relativa accelerazione
    T_xx=avg(nabla_grad(u_i)[0])*avg(nabla_grad(u_i)[0])+avg(nabla_grad(u_r)[0])*avg(nabla_grad(u_r)[0])-avg(nabla_grad(u_i)[1])*avg(nabla_grad(u_i)[1])-avg(nabla_grad(u_r)[1])*avg(nabla_grad(u_r)[1])-avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[2])-avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[2])
    T_xy=2.*(avg(nabla_grad(u_r)[0])*avg(nabla_grad(u_r)[1])+avg(nabla_grad(u_i)[0])*avg(nabla_grad(u_i)[1]))
    T_xz=2.*(avg(nabla_grad(u_r)[0])*avg(nabla_grad(u_r)[2])+avg(nabla_grad(u_i)[0])*avg(nabla_grad(u_i)[2]))
    F_MST_x_0[i]=(eps_fluid_r/4.0)*(assemble((T_xx*cos_xi+ T_xy*cos_yi+T_xz*cos_zi) * dS(i+1)))
    a_MST_x_0[i]=F_MST_x_0[i]/m
    # F_MST_y  e relativa accelerazione
    T_yx=2.*(avg(nabla_grad(u_r)[0])*avg(nabla_grad(u_r)[1])+avg(nabla_grad(u_i)[0])*avg(nabla_grad(u_i)[1]))
    T_yy=avg(nabla_grad(u_i)[1])*avg(nabla_grad(u_i)[1])+avg(nabla_grad(u_r)[1])*avg(nabla_grad(u_r)[1])-avg(nabla_grad(u_i)[0])*avg(nabla_grad(u_i)[0])-avg(nabla_grad(u_r)[0])*avg(nabla_grad(u_r)[0])-avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[2])-avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[2])
    T_yz=2.*(avg(nabla_grad(u_r)[1])*avg(nabla_grad(u_r)[2])+avg(nabla_grad(u_i)[1])*avg(nabla_grad(u_i)[2]))
    F_MST_y_0[i]=(eps_fluid_r/4.0)*(assemble((T_yx*cos_xi+ T_yy*cos_yi+T_yz*cos_zi) * dS(i+1)))
    a_MST_y_0[i]=F_MST_y_0[i]/m
    # F_MST_z  e relativa accelerazione
    T_zx=avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[0])+avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[0])+avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[0])+avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[0])
    T_zy=avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[1])+avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[1])+avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[1])+avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[1])
    T_zz=avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[2])+avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[2])-avg(nabla_grad(u_i)[0])*avg(nabla_grad(u_i)[0])-avg(nabla_grad(u_r)[0])*avg(nabla_grad(u_r)[0])-avg(nabla_grad(u_i)[1])*avg(nabla_grad(u_i)[1])-avg(nabla_grad(u_r)[1])*avg(nabla_grad(u_r)[1])
    F_MST_z_0[i]=(eps_fluid_r/4.0)*(assemble((T_zx*cos_xi+ T_zy*cos_yi+T_zz*cos_zi) * dS(i+1)))
    a_MST_z_0[i]=F_MST_z_0[i]/m
    i=i+1
i=0
while i<=N_particles-1:
  
  #u_x_0[i]=16*u_fluido_max*((centroy[i]/depth_box)*(depth_box-centroy[i])/depth_box) * ((centroz[i]/h_box)*(h_box-centroz[i])/h_box)
  u_x_0[i]=4*u_fluido_max*((centrez[i]/h_box)*(h_box-centrez[i])/h_box)
  v_rel_x_0[i] = (v_x_0[i] - u_x_0[i])
  a_visc_x_0 = -(1/tau)* v_rel_x_0[i]
  
  
  a_visc_y_0 = -(1/tau)* v_y_0[i]
  F_lift_y_0[i] = 6*C*mu*pow(rad,3)*u_fluido_max/(depth_box*(centrey[i]-rad))
  a_lift_y_0[i] = F_lift_y_0[i]/m
  
    
  a_visc_z_0 = -(1/tau)* v_z_0[i]
  F_lift_z_0[i] = 6*C*mu*pow(rad,3)*u_fluido_max/(h_box*(centrez[i]-rad))
  a_lift_z_0[i] = F_lift_z_0[i]/m
  buoyancy_gravity_force=-4.*3.141*pow(rad,3)*acc_g*(particle_density-medium_density)/3.
    
  a_x_0[i] = a_MST_x_0[i] + a_visc_x_0
  a_y_0[i] = a_MST_y_0[i] + a_visc_y_0 + a_lift_y_0[i]
  a_z_0[i] = a_MST_z_0[i] + a_visc_z_0 + a_lift_z_0[i] + buoyancy_gravity_force
  
  #if rank == 0 :
      #if i==0: 
          #print "\n\rInitial values"
      #print "\n\rPARTICLE",i+1
      #print "Volume sph",i+1, "=", assemble(1.*dx(i+1))
      #print "Surface sph",i+1,"=", assemble(Constant(1.0)*dS(i+1))
      #print "centre_x particle",i+1,"=",centrex[i]
      #print "centre_y particle",i+1,"=",centrey[i]
      #print "centre_z particle",i+1,"=",centrez[i]
      #print "v_x_0 particle",i+1,"=", v_x_0[i]
      #print "u_x_0 particle",i+1,"=", u_x_0[i]
      #print "v_rel_x_0 particle",i+1,"=", v_rel_x_0[i]
      #print "v_y_0 particle",i+1,"=", v_y_0[i]
      #print "v_z_0 particle",i+1,"=", v_z_0[i]
      #print "a_MST_x_0 particle",i+1, "=", a_MST_x_0[i]
      #print "a_MST_y_0 particle",i+1,"=", a_MST_y_0[i]
      #print "a_MST_z_0 particle",i+1,"=", a_MST_z_0[i]
      #print "F_x_MST_0 particle",i+1,"=", m*a_MST_x_0[i]
      #print "F_y_MST_0 particle",i+1,"=", m*a_MST_y_0[i]
      #print "F_z_MST_0 particle",i+1,"=", F_MST_z_0[i]

  #else :
       #pass
   
   
  i=i+1

if rank == 0 :
    path="./InputFiles"
    os.chdir(path)
    os.remove(particle_name)
    filexchange=open(particle_name,"w")
    filexchange.write('<Particles >'+'\n')
    i=0
    while i<=N_particles-1:
        filexchange.write('  <Particle XC="'+str(centrex[i])+'" YC="'+str(centrey[i])+'" ZC="'+str(centrez[i])+'" VX="'+str(v_x_0[i])+'" VY="'+str(v_y_0[i])+'" VZ="'+str(v_z_0[i])+'" AX="'+str(a_x_0[i])+'" AY="'+str(a_y_0[i])+'" AZ="'+str(a_z_0[i])+'" />'+"\n")
        
        print "centre_x particle",i+1,"=",centrex[i]
        i+=1
    filexchange.write('</Particles>'+'\n')
    filexchange.close()
    path=".."
    os.chdir(path)
else :
    pass
MPI.barrier(comm)

time_fin=time.time()
if rank == 0 :
    print "DURATION init_diel.py=", time_fin-time_in
    print "END init_diel.py\n"