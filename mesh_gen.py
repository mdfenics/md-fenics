from dolfin import *
from mshr import  *
import sys
import os
import string
import numpy
import xml.etree.ElementTree
import pygmsh as pg
import numpy as np
#import stl
#from stl import mesh
import subprocess
import shutil
import time

#option=1:  out-of-surface integration and project utilization
#option=2:  surface integration and use of projects
#option=3:  out-of-surface integration and use of grad(u)
#option=4:  integration on the surface and use of grad(u)


print("\nSTART mesh_gen.py")

time_in=time.time()

comm = mpi_comm_world()
rank = MPI.rank(comm)
size_mpi= MPI.size(comm)

path="./InputFiles"
os.chdir(path)

tree_J = xml.etree.ElementTree.parse('Job.xml')
root_J = tree_J.getroot()
solver_name=root_J[0].attrib['Value']
geometry_name=root_J[1].attrib['Value']
particle_name=root_J[2].attrib['Value']
time_name=root_J[3].attrib['Value']
# Geometry options
tree_G = xml.etree.ElementTree.parse(geometry_name)
root_G = tree_G.getroot()
N_electrodes=int(root_G[0].attrib['Value'])  #2
larghezza_elettrodi=float(root_G[1].attrib['Value']) #40e-6
L_box=2*N_electrodes*larghezza_elettrodi
h_box=float(root_G[2].attrib['Value']) #100.e-6
depth_box=float(root_G[3].attrib['Value']) #60e-6
L_BC_Neumann=float(root_G[4].attrib['Value']) #40.e-6
L_el_Power=float(root_G[5].attrib['Value']) #40.e-6
Len_car_box=float(root_G[6].attrib['Value']) #4.e-6
rad=float(root_G[7].attrib['Value']) #6.e-6  #raggio

# Solver and integration options
tree_S = xml.etree.ElementTree.parse(solver_name)
root_S = tree_S.getroot()
option=float(root_S[8].attrib['Value'])

tree_P = xml.etree.ElementTree.parse(particle_name)
root_P = tree_P.getroot()
# Read data for Initial Particle Position (Center)

# Time variable
tree_T = xml.etree.ElementTree.parse(time_name)
root_T = tree_T.getroot()
t=float(root_T[0].attrib['Value'])
cycle=int(root_T[1].attrib['Value'])
cycle_out=int(root_T[2].attrib['Value'])

#if rank == 0 :
     #print "\nnumber cycle:", cycle
X0 = []
for particle in root_P:
    X0.append([float(particle.attrib['XC']),float(particle.attrib['YC']),float(particle.attrib['ZC'])])
# rescale time scale pulse
N_particles=len(X0)
path=".."
os.chdir(path)

#option=1:  out-of-surface integration and project utilization
#option=2:  surface integration and use of projects
#option=3:  out-of-surface integration and use of grad(u)
#option=4:  integration on the surface and use of steps(u)

#RADII
R=[]
if option==1 or option==3:
  i=0
  while i<=N_particles-1:
      R.append(rad+0.1e-6)
      i+=1
else:
    i=0
    while i<=N_particles-1:
        R.append(rad)
        i+=1


def generateBox(x0,y0,z0,x1,y1,z1):
    geom = pg.Geometry()
    box = geom.add_box(
             x0, x1,
             y0, y1,
             z0, z1,
             lcar=4.e-6,  #was 3.e-6
             )
    for (item,surf) in enumerate(box.surface_loop.surfaces):
         geom.add_physical_surface(surf,label='BoxSurf%d'%item)
    return (geom,box.surface_loop.id)

def generateBall(x0,r):
    #geom = pg.built_in.Geometry()
    geom = pg.Geometry()
    ball=geom.add_ball(x0, r, with_volume=False, lcar=0.15*r)
    #Surface Box
    for (item,surf) in enumerate(ball.surface_loop.surfaces):
        geom.add_physical_surface(surf,label='SphereSurf%d'%item)
    return geom

def generateVolume(nbSphere, corners):
    geom = pg.Geometry()
    #geom = pg.Geometry()
    index=list(range(1,2*nbSphere+1))
    coupleA=[x for x in index if x%2]
    list(map(lambda x: index.remove(x), coupleA))
    coupleB=index
    listIdxA=''
    listIdxB=''
    for item in range(nbSphere):
        spherename="Sphere%d"%item
        geom.add_raw_code(["Merge '%s.stl';"%spherename])
        #x Internal Volume
        geom.add_raw_code(["Surface Loop(%d)={%d};"%(coupleA[item],item+1)])
        # x Ext Volume
        geom.add_raw_code(["Surface Loop(%d)={-%d};"%(coupleB[item],item+1)])
        if item!=nbSphere-1:
            listIdxA+='%d,'%coupleA[item]
        else:
            listIdxA+='%d'%coupleA[item]
        if item!=nbSphere-1:
            listIdxB+='%d,'%coupleB[item]
        else:
            listIdxB+='%d'%coupleB[item]
        geom.add_raw_code(["Volume(%d)={%d};"%(item+1,coupleA[item])])
        geom.add_raw_code(["Physical Volume(%d)={%d};"%(item+1,item+1)])
        geom.add_raw_code(["Physical Surface(%d)={%d};"%(item+1,item+1)])

    (geomBox,idSurfLoop)=generateBox(*corners)
    geom.add_raw_code(geomBox.get_code())
    geom.add_raw_code(["Volume(%d)={%s,%s};"%(nbSphere+1,idSurfLoop,listIdxB)])
    #Mark the volume.
    geom.add_raw_code(["Physical Volume(%d)={%d};"%(nbSphere+1,nbSphere+1)])
    return geom


def generateVolume_mod(nbSphere,N_el,L_el_G,L_el_P,X_len,Y_len,Z_len,Len_car):
    #geom = pg.built_in.Geometry()
    geom = pg.Geometry()
    index=list(range(1,2*nbSphere+1))
    coupleA=[x for x in index if x%2]
    list(map(lambda x: index.remove(x), coupleA))
    coupleB=index
    listIdxA=''
    listIdxB=''
    for item in range(nbSphere):
        spherename="Sphere%d"%item
        geom.add_raw_code(["Merge '%s.stl';"%spherename])
        #x Internal Volume
        geom.add_raw_code(["Surface Loop(%d)={%d};"%(coupleA[item],item+1)])
        # x Ext Volume
        geom.add_raw_code(["Surface Loop(%d)={-%d};"%(coupleB[item],item+1)])
        if item!=nbSphere-1:
            listIdxA+='%d,'%coupleA[item]
        else:
            listIdxA+='%d'%coupleA[item]
        if item!=nbSphere-1:
            listIdxB+='%d,'%coupleB[item]
        else:
            listIdxB+='%d'%coupleB[item]
        geom.add_raw_code(["Volume(%d)={%d};"%(item+1,coupleA[item])])
        geom.add_raw_code(["Physical Volume(%d)={%d};"%(item+1,item+1)])
        geom.add_raw_code(["Physical Surface(%d)={%d};"%(item+1,item+1)])
#    (geomBox,idSurfLoop)=generateBox(*corners)
#    geom.add_raw_code(geomBox.get_code())
    y0=0
    y1=Y_len

    geom.add_raw_code(["pel0%d=newp;"%(0)])
    geom.add_raw_code(["Point(pel0%d)={%g, %g, %g, %g};"%(0, 0.0,y0,0.,Len_car)])
    geom.add_raw_code(["pel1%d=newp;"%(0)])
    geom.add_raw_code(["Point(pel1%d)={%g, %g, %g, %g};"%(0, 0.0,y1,0.,Len_car)])
    geom.add_raw_code(["pel0%d=newp;"%(2*N_el+1)])
    geom.add_raw_code(["Point(pel0%d)={%g, %g, %g, %g};"%(2*N_el+1, X_len,y0,0.,Len_car)])
    geom.add_raw_code(["pel1%d=newp;"%(2*N_el+1)])
    geom.add_raw_code(["Point(pel1%d)={%g, %g, %g, %g};"%(2*N_el+1, X_len,y1,0.,Len_car)])
    geom.add_raw_code(["pel0z%d=newp;"%(0)])
    geom.add_raw_code(["Point(pel0z%d)={%g, %g, %g, %g};"%(0, 0.0,y0,Z_len,Len_car)])
    geom.add_raw_code(["pel1z%d=newp;"%(0)])
    geom.add_raw_code(["Point(pel1z%d)={%g, %g, %g, %g};"%(0, 0.0,y1,Z_len,Len_car)])
    geom.add_raw_code(["pel0z%d=newp;"%(2*N_el+1)])
    geom.add_raw_code(["Point(pel0z%d)={%g, %g, %g, %g};"%(2*N_el+1, X_len,y0,Z_len,Len_car)])
    geom.add_raw_code(["pel1z%d=newp;"%(2*N_el+1)])
    geom.add_raw_code(["Point(pel1z%d)={%g, %g, %g, %g};"%(2*N_el+1, X_len,y1,Z_len,Len_car)])
    geom.add_raw_code(["lel_y%d=newl;"%(0)])
    geom.add_raw_code(["Line(lel_y%d)={pel1%d,pel0%d};"%(0,0,0)])
    geom.add_raw_code(["lel_y%d=newl;"%(2*N_el+1)])
    geom.add_raw_code(["Line(lel_y%d)={pel1%d,pel0%d};"%(2*N_el+1,2*N_el+1,2*N_el+1)])
    j=1
    x_P=L_el_G/2.0
    x_G=x_P+L_el_P
    while j<=N_el :
        geom.add_raw_code(["pel0%d=newp;"%(2*j-1)])
        geom.add_raw_code(["Point(pel0%d)={%g, %g, %g, %g};"%(2*j-1, x_P,y0,0.,Len_car)])
        geom.add_raw_code(["pel1%d=newp;"%(2*j-1)])
        geom.add_raw_code(["Point(pel1%d)={%g, %g, %g, %g};"%(2*j-1, x_P,y1,0.,Len_car)])
        geom.add_raw_code(["pel0%d=newp;"%(2*j)])
        geom.add_raw_code(["Point(pel0%d)={%g, %g, %g, %g};"%(2*j, x_G,y0,0.,Len_car)])
        geom.add_raw_code(["pel1%d=newp;"%(2*j)])
        geom.add_raw_code(["Point(pel1%d)={%g, %g, %g, %g};"%(2*j, x_G,y1,0.,Len_car)])
        geom.add_raw_code(["lel_y%d=newl;"%(2*j-1)])
        geom.add_raw_code(["Line(lel_y%d)={pel1%d,pel0%d};"%(2*j-1,2*j-1,2*j-1)])
        geom.add_raw_code(["lel_y%d=newl;"%(2*j)])
        geom.add_raw_code(["Line(lel_y%d)={pel1%d,pel0%d};"%(2*j,2*j,2*j)])
        geom.add_raw_code(["lel_xp%d=newl;"%(2*j-1)])
        geom.add_raw_code(["Line(lel_xp%d)={pel0%d,pel0%d};"%(2*j-1,2*j,2*j-1)])
        geom.add_raw_code(["lel_xp%d=newl;"%(2*j)])
        geom.add_raw_code(["Line(lel_xp%d)={pel1%d,pel1%d};"%(2*j,2*j,2*j-1)])
        geom.add_raw_code(["llel%d=newll;"%(2*j)])
        geom.add_raw_code(["Line Loop(llel%d)={lel_xp%d,lel_y%d,-lel_xp%d,-lel_y%d};"%(2*j,2*j-1,2*j,2*j,2*j-1)])
        geom.add_raw_code(["rsel%d=news;"%(2*j)])
        geom.add_raw_code(["Surface(rsel%d)={llel%d};"%(2*j,2*j)])
        x_P=x_G+L_el_G
        x_G=x_P+L_el_P
        j+=1
    j=0
    while j<=N_el :
        geom.add_raw_code(["lel_xg%d=newl;"%(2*j)])
        geom.add_raw_code(["Line(lel_xg%d)={pel0%d,pel0%d};"%(2*j,2*j+1,2*j)])
        geom.add_raw_code(["lel_xg%d=newl;"%(2*j+1)])
        geom.add_raw_code(["Line(lel_xg%d)={pel1%d,pel1%d};"%(2*j+1,2*j+1,2*j)])
        geom.add_raw_code(["llel%d=newll;"%(2*j+1)])
        geom.add_raw_code(["Line Loop(llel%d)={lel_xg%d,lel_y%d,-lel_xg%d,-lel_y%d};"%(2*j+1,2*j,2*j+1,2*j+1,2*j)])
        geom.add_raw_code(["rsel%d=news;"%(2*j+1)])
        geom.add_raw_code(["Surface(rsel%d)={llel%d};"%(2*j+1,2*j+1)])
        j+=1
    surf_t="rsel%d"%(1)
    j=1
    while j<=N_el :
        surf_t=surf_t+", rsel%d, rsel%d"%(2*j,2*j+1)
        j+=1
    surf_g=""
    surf_p=""
    j=1
    while j<=N_el-1 :
        if j == N_el-1 :
            surf_p=surf_p+"rsel%d"%(2*j)
            surf_g=surf_g+"rsel%d"%(2*(j+1))

        else :
            surf_p=surf_p+"rsel%d, "%(2*j)
            surf_g=surf_g+"rsel%d, "%(2*(j+1))
        j+=2
    #Mark electrodes to ground)
    geom.add_raw_code(["Physical Surface(%d)={%s};"%(nbSphere+1,surf_g)])
    #Mark electrodes to power)
    geom.add_raw_code(["Physical Surface(%d)={%s};"%(nbSphere+2,surf_p)])
    line0="lel_xg%d, "%(0)
    line1="lel_xg%d, "%(1)
    j=1
    while j<=N_el :
        line0=line0+"lel_xp%d, "%(2*j-1)
        line1=line1+"lel_xp%d, "%(2*j)
        line0=line0+"lel_xg%d, "%(2*j)
        line1=line1+"lel_xg%d, "%(2*j+1)
        j+=1
    geom.add_raw_code(["lel_00=newl;"])
    geom.add_raw_code(["Line(lel_00)={pel0z0,pel00};"])
    geom.add_raw_code(["lel_0N=newl;"])
    geom.add_raw_code(["Line(lel_0N)={pel0z%d,pel0%d};"%(2*N_el+1,2*N_el+1)])
    geom.add_raw_code(["lel_0Nz=newl;"])
    geom.add_raw_code(["Line(lel_0Nz)={pel0z%d,pel0z%d};"%(2*N_el+1,0)])
    geom.add_raw_code(["llel_y0=newll;"])
    geom.add_raw_code(["Line Loop(llel_y0)={"+line0+"lel_0N,-lel_0Nz,-lel_00};"])
    geom.add_raw_code(["rsel_y0=news;"])
    geom.add_raw_code(["Plane Surface(rsel_y0)={llel_y0};"])
    #Mark Min along y directtion (to be consider as Neumann b.c.)
    geom.add_raw_code(["Physical Surface(%d)={%s};"%(nbSphere+3,"rsel_y0")])
    geom.add_raw_code(["lel_10=newl;"])
    geom.add_raw_code(["Line(lel_10)={pel1z0,pel10};"])
    geom.add_raw_code(["lel_1N=newl;"])
    geom.add_raw_code(["Line(lel_1N)={pel1z%d,pel1%d};"%(2*N_el+1,2*N_el+1)])
    geom.add_raw_code(["lel_1Nz=newl;"])
    geom.add_raw_code(["Line(lel_1Nz)={pel1z%d,pel1z%d};"%(2*N_el+1,0)])
    geom.add_raw_code(["llel_y1=newll;"])
    geom.add_raw_code(["Line Loop(llel_y1)={"+line1+"lel_1N,-lel_1Nz,-lel_10};"])
    geom.add_raw_code(["rsel_y1=news;"])
    geom.add_raw_code(["Plane Surface(rsel_y1)={llel_y1};"])
    #Mark Max along y directtion (to be consider as Neumann b.c.)
    geom.add_raw_code(["Physical Surface(%d)={%s};"%(nbSphere+4,"rsel_y1")])
    geom.add_raw_code(["lel_11z=newl;"])
    geom.add_raw_code(["Line(lel_11z)={pel1z0,pel0z0};"])
    geom.add_raw_code(["lel_NNz=newl;"])
    geom.add_raw_code(["Line(lel_NNz)={pel1z%d,pel0z%d};"%(2*N_el+1,2*N_el+1)])
    geom.add_raw_code(["llel_x0=newll;"])
    geom.add_raw_code(["Line Loop(llel_x0)={lel_00,lel_11z,-lel_10,-lel_y0};"])
    geom.add_raw_code(["rsel_x0=news;"])
    geom.add_raw_code(["Surface(rsel_x0)={llel_x0};"])
    #Mark Min along x directtion (to be consider as periodic b.c.)
    geom.add_raw_code(["Physical Surface(%d)={%s};"%(nbSphere+5,"rsel_x0")])
    geom.add_raw_code(["llel_xN=newll;"])
    geom.add_raw_code(["Line Loop(llel_xN)={lel_0N,lel_NNz,-lel_1N,-lel_y%d};"%(2*N_el+1)])
    geom.add_raw_code(["rsel_xN=news;"])
    geom.add_raw_code(["Surface(rsel_xN)={llel_xN};"])
    #Mark Max along x directtion (to be consider as periodic b.c.)
    geom.add_raw_code(["Physical Surface(%d)={%s};"%(nbSphere+6,"rsel_xN")])
    geom.add_raw_code(["llel_z1=newll;"])
    geom.add_raw_code(["Line Loop(llel_z1)={lel_0Nz,lel_NNz,-lel_1Nz,-lel_11z};"])
    geom.add_raw_code(["rsel_z1=news;"])
    geom.add_raw_code(["Surface(rsel_z1)={llel_z1};"])
    #Mark top z (to be consider an electrode to ground)
    geom.add_raw_code(["Physical Surface(%d)={%s};"%(nbSphere+7,"rsel_z1")])
    idSurfLoop=surf_t+", rsel_xN"+", rsel_z1"+", rsel_x0"+", rsel_y0"+", rsel_y1"
    geom.add_raw_code(["s_box=newsl;"])
    geom.add_raw_code(["Surface Loop(s_box)={%s};"%(idSurfLoop)])
    geom.add_raw_code(["Volume(%d)={s_box,%s};"%(nbSphere+1,listIdxB)])
    #Mark the volume.
    geom.add_raw_code(["Physical Volume(%d)={%d};"%(nbSphere+1,nbSphere+1)])
    return geom


def writeGmsh(filepath,geom ):
     filename = filepath+'.geo'
     if os.path.exists(filepath):
             os.remove(filename)
             os.remove(filepath+'.msh')
     else:
        # this is a workaround for touch the file.
        open(filename, 'a').close()
     # automatically close the file
     with open(filename, 'w') as handler:
             handler.write(geom.get_code())


MPI.barrier(comm)

if cycle % cycle_out==1:
    directoryBase=os.getcwd()
    directory_out_cycles = os.path.join(directoryBase, "Out_%d"%(cycle-1))
    if not os.path.exists(directory_out_cycles):
             os.mkdir(directory_out_cycles)
    else:
             os.stat(directory_out_cycles)
             
# BoxHoles
directoryBase=os.getcwd()
directoryNew = os.path.join(directoryBase, "BoxHoles")
if not os.path.exists(directoryNew):
             os.mkdir(directoryNew)
else:
             os.stat(directoryNew)
            
             
for (item,(x0,r)) in enumerate(zip(X0,R)):
    geom=generateBall(x0,r)
    
    writeGmsh(os.path.join(directoryNew,"Sphere%d"%item),geom)
    
    if cycle % cycle_out==1:
        writeGmsh(os.path.join(directory_out_cycles,"Sphere%d"%item),geom)
      
    subprocess.call(["gmsh -2 Sphere%d.geo -o Sphere%d.stl -v 0"%(item,item)], shell=True, cwd=directoryNew)
    
    if cycle % cycle_out==1: 
        subprocess.call(["gmsh -2 Sphere%d.geo -o Sphere%d.stl -v 0"%(item,item)], shell=True, cwd=directory_out_cycles)
        
boxLimit=(0,0,0,L_box, depth_box, h_box)
#def generateVolume_mod(nbSphere, corners,N_el,L_el_G,L_el_P,L_depth,Len_car):
geom=generateVolume_mod(N_particles,N_electrodes,L_BC_Neumann,L_el_Power,L_box,depth_box,h_box,Len_car_box)
writeGmsh(os.path.join(directoryNew,"VolMesh"),geom)

if cycle % cycle_out==1:
    writeGmsh(os.path.join(directory_out_cycles,"VolMesh"),geom)

# subprocess to create 3D mesh
subprocess.call(["gmsh -3 VolMesh.geo -o VolMesh.msh -v 0"],shell=True,cwd=directoryNew)
#subprocess.call(["gmsh -3 VolMesh.geo -o VolMesh.vtk -v 0"],shell=True,cwd=directoryNew)
subprocess.call(["dolfin-convert VolMesh.msh VolMesh.xml"],shell=True,cwd=directoryNew)
meshDir=os.path.join(os.getcwd(),"BoxHoles")
#print(meshDir)
namemesh="VolMesh"
pathmesh=os.path.join(meshDir,namemesh)
mesh=Mesh(pathmesh+".xml")
fileh=XDMFFile(mpi_comm_world(),meshDir+"/Mesh.xdmf")
fileh.write(mesh)
mv=MeshFunction("size_t",mesh, pathmesh+"_physical_region.xml")
meshSphere=Mesh(SubMesh(mesh,mv,2))
fileh=XDMFFile(mpi_comm_world(),meshDir+"/SubDomain.xdmf")
fileh.write(mesh)
mf=MeshFunction("size_t",mesh, pathmesh+"_facet_region.xml")
hdf = HDF5File(mesh.mpi_comm(), "file.h5", "w")
hdf.write(mesh, "/mesh")
hdf.write(mv, "/mv")
hdf.write(mf, "/mf")
hdf.close()

time_fin=time.time()
print("DURATION mesh_gen.py=", time_fin-time_in)
print("END mesh_gen.py\n")
