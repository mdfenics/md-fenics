from dolfin import *
from mshr import  *
import sys
import os
import string
import numpy
import xml.etree.ElementTree
import pygmsh as pg
import numpy as np
#import stl
#from stl import mesh
import subprocess
import shutil
import time

#option=1:  out-of-surface integration and project utilization
#option=2:  surface integration and use of projects
#option=3:  out-of-surface integration and use of grad(u)
#option=4:  integration on the surface and use of grad(u)


tempo_in=time.time()

comm = mpi_comm_world()
rank = MPI.rank(comm)
size_mpi= MPI.size(comm)


if rank == 0 :
    print "\nSTART cycle_diel.py"
else:
        pass

path="./InputFiles"
os.chdir(path)

tree_J = xml.etree.ElementTree.parse('Job.xml')
root_J = tree_J.getroot()
solver_name=root_J[0].attrib['Value']
geometry_name=root_J[1].attrib['Value']
particle_name=root_J[2].attrib['Value']
time_name=root_J[3].attrib['Value']

# Solver and integration options
tree_S = xml.etree.ElementTree.parse(solver_name)
root_S = tree_S.getroot()

delta_t=float(root_S[0].attrib['Value']) #2.e-5
t_poisson_factor=int(root_S[1].attrib['Value'])
t_poisson = t_poisson_factor*delta_t
linear_solver = root_S[2].attrib['Value']
abs_tol=float(root_S[3].attrib['Value'])
rel_tol=float(root_S[4].attrib['Value'])
div_lim=float(root_S[5].attrib['Value'])
if (root_S[6].attrib['Value'] == 'false') :
   no0_ini= False
else :
   no0_ini= True
if (root_S[7].attrib['Value'] == 'false') :
   verb_log = False
else :
   verb_log = True
option=float(root_S[8].attrib['Value'])

# Time variable
tree_T = xml.etree.ElementTree.parse(time_name)
root_T = tree_T.getroot()
t=float(root_T[0].attrib['Value'])
cycle=int(root_T[1].attrib['Value'])
cycle_out=int(root_T[2].attrib['Value'])
# Geometry options
tree_G = xml.etree.ElementTree.parse(geometry_name)
root_G = tree_G.getroot()
N_electrodes=int(root_G[0].attrib['Value'])  #2

electrodes_width=float(root_G[1].attrib['Value']) #40e-6
L_box=2*N_electrodes*electrodes_width
h_box=float(root_G[2].attrib['Value']) #100.e-6
depth_box=float(root_G[3].attrib['Value']) #60e-6
L_BC_Neumann=float(root_G[4].attrib['Value']) #40.e-6
L_el_Power=float(root_G[5].attrib['Value']) #40.e-6
Len_car_box=float(root_G[6].attrib['Value']) #4.e-6
rad=float(root_G[7].attrib['Value']) #6.e-6  #raggio

tree_P = xml.etree.ElementTree.parse(particle_name)
root_P = tree_P.getroot()

# Read data for Initial Particle Position (Center)

X0 = []
v_x_0 = []
v_y_0 = []
v_z_0 = []
a_x_0 = []
a_y_0 = []
a_z_0 = []
for particle in root_P:
    X0.append([float(particle.attrib['XC']),float(particle.attrib['YC']),float(particle.attrib['ZC'])])
    v_x_0.append(float(particle.attrib['VX']))
    v_y_0.append(float(particle.attrib['VY']))
    v_z_0.append(float(particle.attrib['VZ']))
    a_x_0.append(float(particle.attrib['AX']))
    a_y_0.append(float(particle.attrib['AY']))
    a_z_0.append(float(particle.attrib['AZ']))

N_particles=len(X0)
path=".."
os.chdir(path)
# rescale time scale pulse


#Radius
R=[]
if option==1 or option==3:
  i=0
  while i<=N_particles-1:
      R.append(rad+0.1e-6)
      i+=1
else:
    i=0
    while i<=N_particles-1:
        R.append(rad)
        i+=1


#QUANTITIES FOR THE CALCULATION OF FORCES
mu=0.00131  #dynamic viscosity
m=1.0527e-12   # cell mass
u_fluido_max=100e-6
gamma= 6*3.141*rad*mu
tau= m/gamma   # F = m*a= -gamma*v_rel  =>  a= - gamma*v_rel/m = -v_rel/tau => tau=m/gamma
C=0.172 # lift force factor
acc_g = 9.81
coef=  1./(1.+0.5*(delta_t/tau))  # for the verlet method

#GENERAL ELECTROMAGNETIC QUANTITIES
eps_0 = 8.854187818e-12
V_rms=5
# cross over freq: 50500, sigma_fluid=0.03
frequency=1e6
omega= 2*3.141*frequency


medium_density=1000
eps_fluid_r = 79*eps_0
sigma_fluid=0.03 # whit 0.01:  Refcm=0,85, frequency 1 MHz
eps_fluid_imm=-sigma_fluid/omega

## ELECTROMAGNETIC QUANTITIES MDA-231. Calculation of EPS_EFF
particle_density=1077

eps_mem_r=24*eps_0
sigma_mem=1e-7
eps_mem_imm=-sigma_mem/omega
#print ("eps_mem_imm=",eps_mem_imm)

eps_cyt_r=50*eps_0
sigma_cyt=0.2
eps_cyt_imm=-sigma_cyt/omega

# calculation eps_eff:
#eps_eff = eps_mem*((gamma_cube +2A)/(gamma_cube-A))=eps_mem*rapporto
gamma_cube=pow((rad/(rad-1e-8)),3)
a=eps_cyt_r-eps_mem_r
b=eps_cyt_imm-eps_mem_imm
c=eps_cyt_r+2*eps_mem_r
d=eps_cyt_imm+2*eps_mem_imm
A_r=(a*c+b*d)/(c*c+d*d)
A_i=(b*c-a*d)/(c*c+d*d)

ratio_real=((gamma_cube+2.*A_r)*(gamma_cube-A_r)-2.*A_i*A_i)/(pow((gamma_cube-A_r),2)+A_i*A_i)
ratio_imm=(A_i*3*gamma_cube)/(pow((gamma_cube-A_r),2)+A_i*A_i)
eps_eff_r = eps_mem_r*ratio_real - eps_mem_imm*ratio_imm
eps_eff_imm = eps_mem_imm*ratio_real + eps_mem_r*ratio_imm

#print "eps eff_r=", eps_eff_r/eps_0
#print "eps eff imm=",eps_eff_imm/eps_0

##  F_CM
a=eps_eff_r-eps_fluid_r
b=eps_eff_imm-eps_fluid_imm
c=eps_eff_r+2*eps_fluid_r
d=eps_eff_imm+2*eps_fluid_imm
Re_fcm=(a*c+b*d)/(c*c+d*d)
#print "\nRe_f_CM MDA-231=", Re_fcm


# Subdomain for Periodic boundary condition
class PeriodicBoundary(SubDomain):
    # Left boundary is "target domain" G
    def inside(self, x, on_boundary):
        return bool(x[0] < DOLFIN_EPS and x[0] > -DOLFIN_EPS and on_boundary)
    # Map right boundary (H) to left boundary (G)
    def map(self, x, y):
        y[0] = x[0] - L_box
        y[1] = x[1]
        y[2] = x[2]


class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return (near(x[2], 0) and between(x[0], (x_sx, x_dx)))


# OUTPUT pvd

outfile_header="output"
file_u = File(outfile_header+"_poisson_solution.pvd")

# FILE DI OUTPUT
if rank == 0:
    out_VOL_SPHERE=open(outfile_header+"VOL_SPHERE.csv","a")
    out_SURFACE_SPHERE=open(outfile_header+"SURFACE_SPHERE.csv","a")
    #out_VALORI_INIZIALI=open(outfile_header+"VAL_INIZIALI.csv","a")
    out_CENTRE_x=open(outfile_header+"CENTRE_x.csv","a")
    out_CENTRE_y=open(outfile_header+"CENTRE_y.csv","a")
    out_CENTRE_z=open(outfile_header+"CENTRE_z.csv","a")
    #out_CENTRE_x_LOOP=open(outfile_header+"CENTRE_x_LOOP.csv","a")
    #out_CENTRE_y_LOOP=open(outfile_header+"CENTRE_y_LOOP.csv","a")
    #out_CENTRE_z_LOOP=open(outfile_header+"CENTRE_z_LOOP.csv","a")
    #out_CENTRE_x_old=open(outfile_header+"CENTRE_x_old.csv","a")
    #out_CENTRE_y_old=open(outfile_header+"CENTRE_y_old.csv","a")
    #out_CENTRE_z_old=open(outfile_header+"CENTRE_z_old.csv","a")
    out_ACCEL_MST_x_1=open(outfile_header+"A_MST_x_1.csv","a")
    out_ACCEL_MST_y_1=open(outfile_header+"A_MST_y_1.csv","a")
    out_ACCEL_MST_z_1=open(outfile_header+"A_MST_z_1.csv","a")
    out_A_VISC_x_1=open(outfile_header+"A_VISC_x_1.csv","a")
    out_A_VISC_y_1=open(outfile_header+"A_VISC_y_1.csv","a")
    out_A_VISC_z_1=open(outfile_header+"A_VISC_z_1.csv","a")
    out_A_LIFT_y_1=open(outfile_header+"A_LIFT_y_1.csv","a")
    out_A_LIFT_z_1=open(outfile_header+"A_LIFT_z_1.csv","a")
    out_ACCEL_x_1=open(outfile_header+"ACCEL_x.csv","a")
    out_ACCEL_y_1=open(outfile_header+"ACCEL_y.csv","a")
    out_ACCEL_z_1=open(outfile_header+"ACCEL_z.csv","a")
    #out_VEL_z_0=open(outfile_header+"VEL_z_0.csv","a")
    out_VEL_x_1=open(outfile_header+"VEL_x_1.csv","a")
    out_VEL_y_1=open(outfile_header+"VEL_y_1.csv","a")
    out_VEL_z_1=open(outfile_header+"VEL_z_1.csv","a")
    #out_VEL_REL_x_1=open(outfile_header+"U_x_1.csv","a")
    out_LOOP=open(outfile_header+"LOOP.csv","a")
else :
     pass
# DEFINIZIONI ARRAY

centrex=[]
centrey=[]
centrez=[]

centre_x_old=[]
centre_y_old=[]
centre_z_old=[]
vol_sphere=[]
surface_sphere=[]
#v_x_0=[]
v_x_1=[]
u_x_0=[]
u_x_1=[]
v_rel_x_0 =[]
v_rel_x_1 =[]
#v_y_0=[]
v_y_1=[]
#v_z_0=[]
v_z_1=[]
cos_x=[]
cos_y=[]
cos_z=[]
F_MST_x_0=[]
F_MST_x_1=[]
F_MST_y_0=[]
F_MST_y_1=[]
F_MST_z_0=[]
F_MST_z_1=[]
F_lift_y_0=[]
F_lift_y_1=[]
F_lift_z_0=[]
F_lift_z_1=[]
a_visc_x_0=[]
a_visc_x_1=[]
a_visc_y_0=[]
a_visc_y_1=[]
a_visc_z_0=[]
a_visc_z_1=[]
a_lift_y_0=[]
a_lift_y_1=[]
a_lift_z_0=[]
a_lift_z_1=[]
a_MST_x_0=[]
a_MST_x_1=[]
a_MST_y_0=[]
a_MST_y_1=[]
a_MST_z_0=[]
a_MST_z_1=[]
#a_x_0=[]
a_x_1=[]
#a_y_0=[]
a_y_1=[]
#a_z_0=[]
a_z_1=[]

#INIZIALIZZAZIONE ARRAY
i=1
while i<=N_particles:

  vol_sphere.append(0.)
  surface_sphere.append(0.)
  centrex.append(0.)
  centrey.append(0.)
  centrez.append(0.)
  centre_x_old.append(0.)
  centre_y_old.append(0.)
  centre_z_old.append(0.)
#  v_x_0.append(0)
  v_x_1.append(0)
  u_x_0.append(0.)
  u_x_1.append(0.)
  v_rel_x_0.append(0.)
  v_rel_x_1.append(0.)
#  v_y_0.append(0.)
  v_y_1.append(0.)
#  v_z_0.append(0.)
  v_z_1.append(0.)
  cos_x.append(0.)
  cos_y.append(0.)
  cos_z.append(0.)
  F_MST_x_0.append(0.)
  F_MST_y_0.append(0.)
  F_MST_z_0.append(0.)
  F_MST_x_1.append(0.)
  F_MST_y_1.append(0.)
  F_MST_z_1.append(0.)
  F_lift_y_0.append(0.)
  F_lift_y_1.append(0.)
  F_lift_z_0.append(0.)
  F_lift_z_1.append(0.)
  a_lift_y_0.append(0.)
  a_lift_y_1.append(0.)
  a_lift_z_0.append(0.)
  a_lift_z_1.append(0.)
  a_x_1.append(0.)
  a_y_1.append(0.)
  a_z_1.append(0.)
  a_MST_x_0.append(0.)
  a_MST_y_0.append(0.)
  a_MST_z_0.append(0.)
  a_MST_x_1.append(0.)
  a_MST_y_1.append(0.)
  a_MST_z_1.append(0.)
  a_visc_x_0.append(0.)
  a_visc_y_0.append(0.)
  a_visc_z_0.append(0.)
  a_visc_x_1.append(0.)
  a_visc_y_1.append(0.)
  a_visc_z_1.append(0.)
#  a_x_0.append(0.)
#  a_y_0.append(0.)
#  a_z_0.append(0.)
  i+=1

MPI.barrier(comm)


i=0
while i<=N_particles-1:
    centrex[i]=X0[i][0]
    centrey[i]=X0[i][1]
    centrez[i]=X0[i][2]
    i=i+1


# TIME CYCLE
#t=delta_t

iteration=1
while iteration<=t_poisson_factor:
  if rank == 0 :
     if cycle % cycle_out ==0: 
        print "\nCYCLE number:", cycle
     out_CENTRE_x.write("\n"+str(t)+"\n")
     out_CENTRE_y.write("\n"+str(t)+"\n")
     out_CENTRE_z.write("\n"+str(t)+"\n")
     #out_CENTRE_x_old.write("\n"+str(t)+"\n")
     #out_CENTRE_y_old.write("\n"+str(t)+"\n")
     #out_CENTRE_z_old.write("\n"+str(t)+"\n")
     #out_CENTRE_x_LOOP.write("\n"+str(t)+"\n")
     #out_CENTRE_y_LOOP.write("\n"+str(t)+"\n")
     #out_CENTRE_z_LOOP.write("\n"+str(t)+"\n")
     out_VOL_SPHERE.write("\n"+str(t)+"\n")
     out_SURFACE_SPHERE.write("\n"+str(t)+"\n")
     #out_VEL_z_0.write("\n"+str(t)+"\n")
     out_VEL_x_1.write("\n"+str(t)+"\n")
     out_VEL_y_1.write("\n"+str(t)+"\n")
     out_VEL_z_1.write("\n"+str(t)+"\n")
     out_A_VISC_x_1.write("\n"+str(t)+"\n")
     out_A_VISC_y_1.write("\n"+str(t)+"\n")
     out_A_VISC_z_1.write("\n"+str(t)+"\n")
     out_ACCEL_x_1.write("\n"+str(t)+"\n")
     out_ACCEL_y_1.write("\n"+str(t)+"\n")
     out_ACCEL_z_1.write("\n"+str(t)+"\n")
     out_ACCEL_MST_x_1.write("\n"+str(t)+"\n")
     out_ACCEL_MST_y_1.write("\n"+str(t)+"\n")
     out_ACCEL_MST_z_1.write("\n"+str(t)+"\n")
     out_A_LIFT_y_1.write("\n"+str(t)+"\n")
     out_A_LIFT_z_1.write("\n"+str(t)+"\n")
     #out_VEL_REL_x_1.write("\n"+str(t)+"\n")
     out_LOOP.write("\n\n"+str(t))
  else :
       pass
   
  #CALCULATION NEW POSITION OF THE PARTICLE CENTERS
  i=0
  while i<=N_particles-1:
    centre_x_old[i]=centrex[i]
    centrex[i]= centrex[i] + v_x_0[i]*delta_t + 0.5* a_x_0[i]*pow(delta_t,2)
    centre_y_old[i]=centrey[i]
    centrey[i]= centrey[i] + v_y_0[i]*delta_t + 0.5*a_y_0[i] *pow(delta_t,2)
    centre_z_old[i]=centrez[i]
    centrez[i]= centrez[i] + v_z_0[i]*delta_t + 0.5*a_z_0[i] *pow(delta_t,2)
    #if rank == 0:
       #out_CENTRE_x_old.write(str(i+1)+", "+ str(centre_x_old[i])+"\n")
       #out_CENTRE_y_old.write(str(i+1)+", "+ str(centre_y_old[i])+"\n")
       #out_CENTRE_z_old.write(str(i+1)+", "+ str(centre_z_old[i])+"\n")
    #else :
         #pass
    i=i+1

  #CONDITIONS ON IMPACT WITH WALLS
  time_in_loop=time.clock()
  loop_number=1
  j=3
  while 2<j:
    exit=True
    if rank == 0:
       out_LOOP.write("\n"+str(loop_number)+","+"loop")
    else :
         pass
    i=0
    while i<=N_particles-1:
  ## if 1
        if (centrex[i]+R[i])>= L_box:
            centrex[i]=R[i]+1.e-9
            if rank == 0 :
               out_LOOP.write("if 1 per la particella:"+str(i+1))
               #print "if 1"
            else :
                 pass
            exit=False
  ##if 2
        if (centrey[i]+R[i])>= depth_box-1.e-9:
            v_y_0[i]=-v_y_0[i]
            centrey[i]=centre_y_old[i]
            if rank == 0 :
               out_LOOP.write("\nif 2 per la particella:"+str(i+1))
               #print "if 2"
            else :
                 pass
            exit=False
  ## if 3
        if (centrey[i]-R[i])<=1.e-9:
            v_y_0[i]=-v_y_0[i]
            centrey[i]=centre_y_old[i]
            if rank == 0 :
               out_LOOP.write("\nif 3 per la particella:"+str(i+1))
               #print "if 3"
            else :
                 pass
            exit=False
  ## if 4
        if (centrez[i]+R[i])>= h_box-1.e-9:
            v_z_0[i]=-v_z_0[i]
            centrez[i]=centre_z_old[i]
            if rank == 0 :
               out_LOOP.write("\nif 4 per la particella:"+str(i+1))
               #print "if 4"
            else :
                 pass
            exit=False
  ## if 5
        if (centrez[i]-R[i])<= 1.e-9:
            v_z_0[i]=-v_z_0[i]
            centrez[i]=centre_z_old[i]
            if rank == 0 :
               out_LOOP.write("\nif 5 per la particella:"+str(i+1))
               #print "if 5"
            else :
                 pass
            exit=False

        i=i+1

    # CONDITIONS ON OVERLAP

    i=0
    while i<=N_particles-2:
        k=i+1

        while k<= N_particles-1:
            delta_x=abs(centrex[i]-centrex[k])
            delta_y=abs(centrey[i]-centrey[k])
            delta_z=abs(centrez[i]-centrez[k])
            D=pow(delta_x*delta_x + delta_y*delta_y + delta_z*delta_z,0.5)
            control_distance= 2.*R[i] + 0.1e-6

            if D <= control_distance:
                exit=False
                #print "D=",D
                cos_dir_x=delta_x/D
                cos_dir_y=delta_y/D
                cos_dir_z=delta_z/D
                increased_radius= (2.*R[i] + 0.15e-6)/2.
        ## if 6
                if centrex[i]>centrex[k]:
                    centrex[i]=centrex[i]+0.5*(2.*increased_radius-D)*cos_dir_x
                    centrex[k]=centrex[k]-0.5*(2.*increased_radius-D)*cos_dir_x
                    if rank == 0:
                       out_LOOP.write("\nif 6 for the particle:"+str(i+1)+" con la particella:"+str(k+1))
                       #print "if 6"
                    else :
                         pass
          ##else 1
                else:
                    centrex[i]=centrex[i]-0.5*(2.*increased_radius-D)*cos_dir_x
                    centrex[k]=centrex[k]+0.5*(2.*increased_radius-D)*cos_dir_x
                    if rank == 0:
                       out_LOOP.write("\nelse 1 for the particle:"+str(i+1)+" whit particle:"+str(k+1))
                       #print "else 1"
                    else :
                         pass
            ## if 7
                if centrey[i]>centrey[k]:
                    centrey[i]=centrey[i]+0.5*(2.*increased_radius-D)*cos_dir_y
                    centrey[k]=centrey[k]-0.5*(2.*increased_radius-D)*cos_dir_y
                    if rank == 0:
                       out_LOOP.write("\nif 7 for the particle:"+str(i+1)+" whit particle:"+str(k+1))
                       #print "if 7"
                    else :
                         pass
           ## else 2
                else:
                    centrey[i]=centrey[i]-0.5*(2.*increased_radius-D)*cos_dir_y
                    centrey[k]=centrey[k]+0.5*(2.*increased_radius-D)*cos_dir_y
                    if rank == 0:
                       out_LOOP.write("\nelse 2 for the particle:"+str(i+1)+" whit particle:"+str(k+1))
                       #print "else 2"
                    else :
                         pass
          ## if 8
                if centrez[i]>centrez[k]:

                    centrez[i]=centrez[i]+0.5*(2.*increased_radius-D)*cos_dir_z
                    centrez[k]=centrez[k]-0.5*(2.*increased_radius-D)*cos_dir_z
                    if rank == 0:
                       out_LOOP.write("\nif 8 for the particle:"+str(i+1)+" whit particle:"+str(k+1))
                       #print "if 8"
                    else :
                         pass
            ##else 3
                else:
                    centrez[i]=centrez[i]-0.5*(2.*increased_radius-D)*cos_dir_z
                    centrez[k]=centrez[k]+0.5*(2.*increased_radius-D)*cos_dir_z
                    if rank == 0:
                       out_LOOP.write("\nelse 3 for the particle:"+str(i+1)+" whit particle:"+str(k+1))
                       #print "else 3"
                    else :
                         pass
            k=k+1

        i+=1
    #if rank == 0:
        #i=0
        #while i<=N_particles-1:
              #out_CENTRE_x_LOOP.write(str(i+1)+", "+ str(centrex[i])+"\n")
              #out_CENTRE_y_LOOP.write(str(i+1)+", "+ str(centrey[i])+"\n")
              #out_CENTRE_z_LOOP.write("loop "+str(loop_number)+",particle "+str(i+1)+": "+ str(centrez[i])+"\n")
              #i=i+1
    #else :
         #pass
    loop_number=loop_number+1
    if loop_number>100:
        time_end_loop=time.clock()
        time_loop=time_end_loop-time_in_loop
        print "time_loop=", time_loop
        quit()
    if exit==True:
      break
  if rank == 0 :
      i=0
      while i<=N_particles-1:
        out_CENTRE_x.write(str(i+1)+", "+ str(centrex[i])+"\n")
        out_CENTRE_y.write(str(i+1)+", "+ str(centrey[i])+"\n")
        out_CENTRE_z.write(str(i+1)+", "+ str(centrez[i])+"\n")
        i=i+1
      if cycle % cycle_out ==0:
        print "Control passed"
  else :
       pass

  directoryBase=os.getcwd()  # rank =0??
  directory_out_cicli = os.path.join(directoryBase, "Out_%d"%cycle)
  
  # PROGRAM RE-PROPOSING
  if iteration == 1:
    #Define Center
    i=0
    while i<=N_particles-1:

        X0[i][0]=centrex[i]
        X0[i][1]=centrey[i]
        X0[i][2]=centrez[i]
        i=i+1
    MPI.barrier(comm)
    parameters["ghost_mode"] = "shared_vertex"
    mesh = Mesh()
    hdf = HDF5File(mesh.mpi_comm(), "file.h5", "r")
    hdf.read(mesh, "/mesh", False)
    mv = MeshFunction("size_t", mesh, mesh.topology().dim()) #CellFunction
    hdf.read(mv, "/mv")
    mf = MeshFunction("size_t", mesh, mesh.topology().dim()-1) #FacetFunction
    hdf.read(mf, "/mf")
    hdf.close()
    dx = Measure("dx", domain=mesh, subdomain_data=mv)
    ds = Measure("ds", domain=mesh, subdomain_data=mf)
    dS = Measure("dS", domain=mesh, subdomain_data=mf)

    degree_L=2
    
    Pot_r = FiniteElement("CG", mesh.ufl_cell(), degree_L-1)
    Pot_i = FiniteElement("CG", mesh.ufl_cell(), degree_L-1)
    Pot_c = Pot_r *Pot_i
    #W = FunctionSpace(mesh,Pot_c,constrained_domain=PeriodicBoundary())
    W = FunctionSpace(mesh,Pot_c)
    bcs = [DirichletBC(W.sub(0), 0.0, mf, N_particles+7), DirichletBC(W.sub(0), V_rms, mf, N_particles+2), DirichletBC(W.sub(0), 0.0, mf, N_particles+1),DirichletBC(W.sub(1), 0.0, mf, N_particles+7),DirichletBC(W.sub(1), 0.0, mf, N_particles+2),DirichletBC(W.sub(1), 0.0, mf, N_particles+1)]
    P = TrialFunction(W)
    P_r, P_i = split(P)
    (v_r, v_i) = TestFunctions(W)
    f_r = Constant(0.0)
    f_i = Constant(0.0)
    dx = Measure("dx", domain=mesh, subdomain_data=mv)
    ds = Measure("ds", domain=mesh, subdomain_data=mf)
    dS = Measure("dS", domain=mesh, subdomain_data=mf)
    #Define variational form
    #IF PER LA FUNZIONE EPS_R e EPS_IMM
    if option==1 or option==3:
       V1=FiniteElement("CG", mesh.ufl_cell(), degree_L-1)
       #V=FunctionSpace(mesh,V1,constrained_domain=PeriodicBoundary())
       V=FunctionSpace(mesh,V1)
       dofmap = V.dofmap()
       my_first, my_last = dofmap.ownership_range() # global
       coord_mesh = V.tabulate_dof_coordinates().reshape((-1, 3))
       unowned = dofmap.local_to_global_unowned()
       dofs = filter(lambda dof: dofmap.local_to_global_index(dof) not in unowned, xrange(my_last-my_first))
       coord_mesh=coord_mesh[dofs]
       #eps_eff_r=eps_fluid_r
       #eps_eff_imm=eps_fluid_imm
       #PARTE REALE DI EPSILON
       eps_r = Function(V)
       eps_r = interpolate(Constant(eps_fluid_r), V)
       eps_r_array = eps_r.vector().get_local()
       i=0
       while i<=N_particles-1:
             for dof in dofs:
                 point=coord_mesh[dof]
                 distance = pow(pow(X0[i][0]-point[0],2)+pow(X0[i][1]-point[1],2)+pow(X0[i][2]-point[2],2),0.5)
                 if (distance < rad):
                      eps_r_array[dof] = eps_eff_r
             i+=1
       eps_r.vector().set_local(eps_r_array)
       
       #Imaginary part of eps
       eps_imm = Function(V)
       eps_imm = interpolate(Constant(eps_fluid_imm), V)
       eps_imm_array = eps_r.vector().get_local()
       i=0
       while i<=N_particles-1:
          for dof in dofs :
              point=coord_mesh[dof]
              distance = pow(pow(X0[i][0]-point[0],2)+pow(X0[i][1]-point[1],2)+pow(X0[i][2]-point[2],2),0.5)
              if (distance < rad):
                eps_imm_array[dof] = eps_eff_imm
          i+=1
       eps_imm.vector().set_local(eps_imm_array)
       a_r = inner(eps_r*grad(P_r),grad(v_r))*dx - inner(eps_imm*grad(P_i),grad(v_r))*dx
       a_i = inner(eps_imm*grad(P_r),grad(v_i))*dx + inner(eps_r*grad(P_i),grad(v_i))*dx
       L_r = f_r*v_r*dx- f_i*v_i*dx
       L_i = f_r*v_i*dx+ f_i*v_r*dx

       a = a_r + a_i
       L = L_r + L_i
       u = Function(W)
       problem = LinearVariationalProblem(a, L, u, bcs)
       solver = LinearVariationalSolver(problem)
        # Set linear solver parameters
       prm = solver.parameters
       if linear_solver == 'Krylov':
           prm.linear_solver = 'gmres'
           if size_MPI == 1: # serial
              prm.preconditioner = 'ilu'
           else :            # parellel
                prm.preconditioner = 'hypre_euclid'
           prm.krylov_solver.absolute_tolerance = abs_tol
           prm.krylov_solver.relative_tolerance = rel_tol
           prm.krylov_solver.divergence_limit = div_lim
           prm.krylov_solver.nonzero_initial_guess=no0_ini
       else:
             prm.linear_solver = 'lu'
       solver.solve()
  #       a = assemble(a_r + a_i)
  #       L = assemble(L_r + L_i)
  #       for bc in bcs: bc.apply(a,L)
  #       u = Function(W)
  #       solve (a, u.vector(), L)
       u_r,u_i = split(u)
       file = File("Solution.pvd")
       file << u
       
       #file = File("eps_imm.pvd")
       #file << eps_imm
       
       #file = File("eps_r.pvd")
       #file << eps_r
       
       if cycle % cycle_out==1 and cycle !=1: 
          file = File("Solution_cycle_%d_.pvd"%(cycle-1))
          file << u
          #file = File("eps_imm_cycle%d.pvd"%(cycle-1))
          #file << eps_imm
          #file = File("eps_r_cycle%d.pvd"%(cycle-1))
          #file << eps_r 
       
       
    else:
       a_r = inner(eps_fluid_r*grad(P_r),grad(v_r))*dx(N_particles+1) - inner(eps_fluid_imm*grad(P_i),grad(v_r))*dx(N_particles+1)
       a_i = inner(eps_fluid_imm*grad(P_r),grad(v_i))*dx(N_particles+1) + inner(eps_fluid_r*grad(P_i),grad(v_i))*dx(N_particles+1)
       L_r=f_r*v_r*dx(N_particles+1)- f_i*v_i*dx(N_particles+1)
       L_i=f_r*v_i*dx(N_particles+1)+ f_i*v_r*dx(N_particles+1)
       
       i=1
       while i<=N_particles:
           a_r = a_r + inner(eps_eff_r*grad(P_r),grad(v_r))*dx(i) - inner(eps_eff_imm*grad(P_i),grad(v_r))*dx(i)
           a_i = a_i + inner(eps_eff_imm*grad(P_r),grad(v_i))*dx(i) + inner(eps_eff_r*grad(P_i),grad(v_i))*dx(i)
           #L_r = L_r + inner(f_r,v_r)*dx(i) - inner(f_i,v_i)*dx(i)
           #L_i = L_i + inner(f_r,v_i)*dx(i) + inner(f_i,v_r)*dx(i)
           L_r = L_r + f_r*v_r*dx(i)- f_i*v_i*dx(i)
           L_i = L_i + f_r*v_i*dx(i)+ f_i*v_r*dx(i)
           i+=1
       a = a_r + a_i
       L = L_r + L_i
       u = Function(W)
       problem = LinearVariationalProblem(a, L, u, bcs)
       solver = LinearVariationalSolver(problem)
        # Set linear solver parameters
       prm = solver.parameters
       if linear_solver == 'Krylov':
           prm.linear_solver = 'gmres'
           if size_MPI == 1: # serial
              prm.preconditioner = 'ilu'
           else :            # parellel
                prm.preconditioner = 'hypre_euclid'
           prm.krylov_solver.absolute_tolerance = abs_tol
           prm.krylov_solver.relative_tolerance = rel_tol
           prm.krylov_solver.divergence_limit = div_lim
           prm.krylov_solver.nonzero_initial_guess=no0_ini
       else:
             prm.linear_solver = 'lu'
       solver.solve()
  #       a = assemble(a_r+ a_i)
  #       L = assemble(L_r+ L_i)
  #       for bc in bcs: bc.apply(a,L)
  #       u = Function(W)
  #       solve (a, u.vector(), L)
       u_r,u_i = split(u)
       file = File("Solution.pvd")
       file << u
    i=0
    while i<=N_particles-1:
        if option==1 or option==2:
           if rank == 0 :
              time_start_project=time.clock()
              print "time_start_project=", time_start_project
           else :
                pass
           VFS = VectorFunctionSpace(mesh, 'CG', 1)
           E_r = project(as_vector(-nabla_grad(u_r)), VFS)
           E_i = project(as_vector(-nabla_grad(u_i)), VFS)
           file = File("Electric field_real.pvd")
           file << E_r
           file = File("Electric field_imm_.pvd")
           file << E_i
           if rank == 0 :
               tempo_fine_project=time.clock()
               print "time_end_project=", time_end_project
               time_project=time_end_project-time_start_project
               print "time_project=", time_project
           else :
               pass
           i=0
           while i<=N_particles-1:
                 
                 cos_xi=Expression("(x[0]-x_c)/pow((x[0]-x_c)*(x[0]-x_c)+(x[1]-y_c)*(x[1]-y_c)+(x[2]-z_c)*(x[2]-z_c),0.5)", x_c=centrex[i], y_c=centrey[i], z_c=centrez[i] ,degree=2)
                 cos_yi=Expression("(x[1]-y_c)/pow((x[0]-x_c)*(x[0]-x_c)+(x[1]-y_c)*(x[1]-y_c)+(x[2]-z_c)*(x[2]-z_c),0.5)",x_c=centrex[i], y_c=centrey[i], z_c=centrez[i] ,degree=2)
                 cos_zi=Expression("(x[2]-z_c)/pow((x[0]-x_c)*(x[0]-x_c)+(x[1]-y_c)*(x[1]-y_c)+(x[2]-z_c)*(x[2]-z_c),0.5)", x_c=centrex[i], y_c=centrey[i], z_c=centrez[i] ,degree=2)
                 
                 
                 T_xx=E_i[0]*E_i[0]+E_r[0]*E_r[0]-E_i[1]*E_i[1]-E_r[1]*E_r[1]-E_i[2]*E_i[2]-E_r[2]*E_r[2]
                 T_xy=E_r[0]*E_r[1]+E_r[0]*E_r[1]+E_i[0]*E_i[1]+E_i[0]*E_i[1]
                 T_xz=E_r[0]*E_r[2]+E_r[0]*E_r[2]+E_i[0]*E_i[2]+E_i[0]*E_i[2]
                 F_MST_x_1[i]=(eps_fluid_r/4.0)*(assemble((T_xx*cos_xi+ T_xy*cos_yi+T_xz*cos_zi)*dS(i+1)))
                 a_MST_x_1[i]=F_MST_x_1[i]/m
                 
                 
                 T_yx=E_r[1]*E_r[0]+E_r[1]*E_r[0]+E_i[1]*E_i[0]+E_i[1]*E_i[0]
                 T_yy=E_i[1]*E_i[1]+E_r[1]*E_r[1]-E_i[0]*E_i[0]-E_r[0]*E_r[0]-E_i[2]*E_i[2]-E_r[2]*E_r[2]
                 T_yz=E_r[1]*E_r[2]+E_r[1]*E_r[2]+E_i[1]*E_i[2]+E_i[1]*E_i[2]
                 F_MST_y_1[i]=(eps_fluid_r/4.0)*(assemble((T_yx*cos_xi+ T_yy*cos_yi+T_yz*cos_zi) * dS(i+1)))
                 a_MST_y_1[i]=F_MST_y_1[i]/m
                 
                 
                 T_zx=E_r[2]*E_r[0]+E_r[2]*E_r[0]+E_i[2]*E_i[0]+E_i[2]*E_i[0]
                 T_zy=E_r[2]*E_r[1]+E_r[2]*E_r[1]+E_i[2]*E_i[1]+E_i[2]*E_i[1]
                 T_zz=E_i[2]*E_i[2]+E_r[2]*E_r[2]-E_i[0]*E_i[0]-E_r[0]*E_r[0]-E_i[1]*E_i[1]-E_r[1]*E_r[1]
                 F_MST_z_1[i]=(eps_fluid_r/4.0)*(assemble((T_zx*cos_xi+ T_zy*cos_yi+T_zz*cos_zi) * dS(i+1)))
                 a_MST_z_1[i]=F_MST_z_1[i]/m
                 i=i+1
        else:
             i=0
             while i<=N_particles-1:
                
                cos_xi=Expression("(x[0]-x_c)/pow((x[0]-x_c)*(x[0]-x_c)+(x[1]-y_c)*(x[1]-y_c)+(x[2]-z_c)*(x[2]-z_c),0.5)", x_c=centrex[i], y_c=centrey[i], z_c=centrez[i] ,degree=2)
                cos_yi=Expression("(x[1]-y_c)/pow((x[0]-x_c)*(x[0]-x_c)+(x[1]-y_c)*(x[1]-y_c)+(x[2]-z_c)*(x[2]-z_c),0.5)",x_c=centrex[i], y_c=centrey[i], z_c=centrez[i] ,degree=2)
                cos_zi=Expression("(x[2]-z_c)/pow((x[0]-x_c)*(x[0]-x_c)+(x[1]-y_c)*(x[1]-y_c)+(x[2]-z_c)*(x[2]-z_c),0.5)", x_c=centrex[i], y_c=centrey[i], z_c=centrez[i] ,degree=2)
                
                
                T_xx=avg(nabla_grad(u_i)[0])*avg(nabla_grad(u_i)[0])+avg(nabla_grad(u_r)[0])*avg(nabla_grad(u_r)[0])-avg(nabla_grad(u_i)[1])*avg(nabla_grad(u_i)[1])-avg(nabla_grad(u_r)[1])*avg(nabla_grad(u_r)[1])-avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[2])-avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[2])
                T_xy=2.*(avg(nabla_grad(u_r)[0])*avg(nabla_grad(u_r)[1])+avg(nabla_grad(u_i)[0])*avg(nabla_grad(u_i)[1]))
                T_xz=2.*(avg(nabla_grad(u_r)[0])*avg(nabla_grad(u_r)[2])+avg(nabla_grad(u_i)[0])*avg(nabla_grad(u_i)[2]))
                F_MST_x_1[i]=(eps_fluid_r/4.0)*(assemble((T_xx*cos_xi+ T_xy*cos_yi+T_xz*cos_zi) * dS(i+1)))
                a_MST_x_1[i]=F_MST_x_1[i]/m
                
                
                T_yx=2.*(avg(nabla_grad(u_r)[0])*avg(nabla_grad(u_r)[1])+avg(nabla_grad(u_i)[0])*avg(nabla_grad(u_i)[1]))
                T_yy=avg(nabla_grad(u_i)[1])*avg(nabla_grad(u_i)[1])+avg(nabla_grad(u_r)[1])*avg(nabla_grad(u_r)[1])-avg(nabla_grad(u_i)[0])*avg(nabla_grad(u_i)[0])-avg(nabla_grad(u_r)[0])*avg(nabla_grad(u_r)[0])-avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[2])-avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[2])
                T_yz=2.*(avg(nabla_grad(u_r)[1])*avg(nabla_grad(u_r)[2])+avg(nabla_grad(u_i)[1])*avg(nabla_grad(u_i)[2]))
                F_MST_y_1[i]=(eps_fluid_r/4.0)*(assemble((T_yx*cos_xi+ T_yy*cos_yi+T_yz*cos_zi) * dS(i+1)))
                a_MST_y_1[i]=F_MST_y_1[i]/m
                
                
                T_zx=avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[0])+avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[0])+avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[0])+avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[0])
                T_zy=avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[1])+avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[1])+avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[1])+avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[1])
                T_zz=avg(nabla_grad(u_i)[2])*avg(nabla_grad(u_i)[2])+avg(nabla_grad(u_r)[2])*avg(nabla_grad(u_r)[2])-avg(nabla_grad(u_i)[0])*avg(nabla_grad(u_i)[0])-avg(nabla_grad(u_r)[0])*avg(nabla_grad(u_r)[0])-avg(nabla_grad(u_i)[1])*avg(nabla_grad(u_i)[1])-avg(nabla_grad(u_r)[1])*avg(nabla_grad(u_r)[1])
                F_MST_z_1[i]=(eps_fluid_r/4.0)*(assemble((T_zx*cos_xi+ T_zy*cos_yi+T_zz*cos_zi) * dS(i+1)))
                a_MST_z_1[i]=F_MST_z_1[i]/m
                i=i+1
    t_poisson = t_poisson + t_poisson_factor*delta_t
    
    # RELATIVE SPEED AND VISCOUS ACCELERATION
    # coef=  1./(1.+0.5*(delta_t/tau))
  i=0
  while i<=N_particles-1:
    u_x_1[i]= 4. * u_fluido_max * (centrez[i]/h_box)*((h_box-centrez[i])/h_box)
    v_x_1[i]=coef*(v_x_0[i] + 0.5*delta_t *a_x_0[i] + 0.5*delta_t*a_MST_x_1[i]+ 0.5 *(delta_t/tau)*u_x_1[i])
    v_rel_x_1[i]=v_x_1[i]-u_x_1[i]
    a_visc_x_1[i]= - (1/tau)* v_rel_x_1[i]
    
    
    F_lift_y_1[i] = 6.*C*mu*pow(rad,3)*u_fluido_max/(depth_box*(centrey[i]-rad))
    a_lift_y_1[i]=F_lift_y_1[i]/m
    v_y_1[i] = coef*(v_y_0[i] + 0.5*delta_t * a_y_0[i] + 0.5*delta_t* a_MST_y_1[i])
    a_visc_y_1[i]= - (1/tau)* v_y_1[i]
    
    
    F_lift_z_1[i] = 6.*C*mu*pow(rad,3)*u_fluido_max/(h_box*(centrez[i]-rad))
    a_lift_z_1[i]=F_lift_z_1[i]/m
    v_z_1[i] = coef*(v_z_0[i] + 0.5*delta_t * a_z_0[i] + 0.5*delta_t* a_MST_z_1[i])
    a_visc_z_1[i]= - (1./tau)* v_z_1[i]
    buoyancy_gravity_force=-4.*3.141*pow(rad,3)*acc_g*(particle_density-medium_density)/3.
    # TOTAL ACCELERATIONS
    a_x_1[i] = a_MST_x_1[i] + a_visc_x_1[i]
    a_y_1[i] = a_MST_y_1[i] +  a_visc_y_1[i] + a_lift_y_1[i]
    a_z_1[i] = a_MST_z_1[i] + a_lift_z_1[i]+  a_visc_z_1[i]+ buoyancy_gravity_force
    
    # INTEGRALS
    vol_sphere[i]= assemble(1.*dx(i+1))
    surface_sphere[i]= assemble(Constant(1.0)*dS(i+1))
    # FILE OUTPUT
    if rank == 0 :
        out_VOL_SPHERE.write(str(i+1)+", "+ str(vol_sphere[i])+"\n")
        out_SURFACE_SPHERE.write(str(i+1)+", "+ str(surface_sphere[i])+"\n")
        out_ACCEL_MST_x_1.write(str(i+1)+", "+ str(a_MST_x_1[i])+"\n")
        out_ACCEL_MST_y_1.write(str(i+1)+", "+ str(a_MST_y_1[i])+"\n")
        out_ACCEL_MST_z_1.write(str(i+1)+", "+ str(a_MST_z_1[i])+"\n")
        out_A_VISC_x_1.write(str(i+1)+", "+ str(a_visc_x_1[i])+"\n")
        out_A_VISC_y_1.write(str(i+1)+", "+ str(a_visc_y_1[i])+"\n")
        out_A_VISC_z_1.write(str(i+1)+", "+ str(a_visc_z_1[i])+"\n")
        out_A_LIFT_y_1.write(str(i+1)+", "+ str(a_lift_y_1[i])+"\n")
        out_A_LIFT_z_1.write(str(i+1)+", "+ str(a_lift_z_1[i])+"\n")
        out_ACCEL_x_1.write(str(i+1)+", "+ str(a_x_1[i])+"\n")
        out_ACCEL_y_1.write(str(i+1)+", "+ str(a_y_1[i])+"\n")
        out_ACCEL_z_1.write(str(i+1)+", "+ str(a_z_1[i])+"\n")
        #out_VEL_z_0.write(str(i+1)+", "+ str(v_z_0[i])+"\n")
        out_VEL_x_1.write(str(i+1)+", "+ str(v_x_1[i])+"\n")
        out_VEL_y_1.write(str(i+1)+", "+ str(v_y_1[i])+"\n")
        out_VEL_z_1.write(str(i+1)+", "+ str(v_z_1[i])+"\n")
        #out_VEL_REL_x_1.write(str(i+1)+", "+ str(u_x_1[i])+"\n")
        #print "\n\rPARTICLE", i+1
        #print "vol sphere",i+1, "=", vol_sphere[i]
        #print "surface sphere",i+1,"=", surface_sphere[i]
        #print "\n\rcentre_x(",t,") particella",i+1,"=",centrex[i]
        #print "\n\rcentre_y(",t,") particella",i+1,"=",centrey[i]
        #print "\n\rcentre_z(",t,") particella",i+1,"=",centrez[i]
        #print "\n\rv_x_0(",t,")particella",i+1,"=", v_x_0[i]
        #print "\n\rv_y_0(",t,")particella",i+1,"=", v_y_0[i]
        #print "\n\rv_z_0(",t,")particella",i+1,"=", v_z_0[i]
        #print "\n\rv_x_1(",t,")particella",i+1,"=", v_x_1[i]
        #print "\n\rv_y_1(",t,")particella",i+1,"=", v_y_1[i]
        #print "\n\rv_z_1(",t,")particella",i+1,"=", v_z_1[i]
        #print "\n\ra_x_0(",t,") particella",i+1,"=", a_x_0[i]
        #print "\n\ra_y_0(",t,") particella",i+1,"=", a_y_0[i]
        #print "\n\ra_z_0(",t,") particella",i+1,"=", a_z_0[i]
        #print "\n\ra_x_1(",t,") particella",i+1,"=", a_x_1[i]
        #print "\n\ra_y_1(",t,") particella",i+1,"=", a_y_1[i]
        #print "\n\ra_z_1(",t,") particella",i+1,"=", a_z_1[i]
        #print "\nF_MST_x_1 particle",i+1,"=", F_MST_x_1[i]
        #print "F_MST_y_1 particele",i+1,"=", F_MST_y_1[i]
        #print "F_MST_z_1 particele",i+1,"=", F_MST_z_1[i]
    #else :
         #pass
         
    v_x_0[i]=v_x_1[i]
    v_y_0[i]=v_y_1[i]
    v_z_0[i]=v_z_1[i]
    a_x_0[i]=a_x_1[i]
    a_y_0[i]=a_y_1[i]
    a_z_0[i]=a_z_1[i]
    i+=1
  iteration=iteration+1
  
  if rank == 0 :
    if cycle % cycle_out ==0:  
        #print "cycle number just completed:",cycle
        print "t at the cycle", cycle, "=", t
    
  else :
     pass
  
  
  cycle = cycle+1 
  
  t=t+delta_t
  
# Storage variables
if rank == 0 :
    path="./InputFiles"
    os.chdir(path)
    os.remove(particle_name)
    filexchange=open(particle_name,"w")
    filexchange.write('<Particles >'+'\n')
    
    i=0
    while i<=N_particles-1:
        filexchange.write('  <Particle XC="'+str(centrex[i])+'" YC="'+str(centrey[i])+'" ZC="'+str(centrez[i])+'" VX="'+str(v_x_0[i])+'" VY="'+str(v_y_0[i])+'" VZ="'+str(v_z_0[i])+'" AX="'+str(a_x_0[i])+'" AY="'+str(a_y_0[i])+'" AZ="'+str(a_z_0[i])+'" />'+"\n")
        i+=1
    filexchange.write('</Particles>'+'\n')
    filexchange.close()
    os.remove(time_name)
    filexchange=open(time_name,"w")
    filexchange.write('<TimeCycleParameters >'+'\n')
    filexchange.write('  <Time Value="'+str(t)+'" />'+"\n")
    filexchange.write('  <Cycle Value="'+str(cycle)+'" />'+"\n")
    filexchange.write('  <Cycle_out Value="'+str(cycle_out)+'" />'+"\n")
    filexchange.write('</TimeCycleParameters>'+'\n')
    filexchange.close()
    path=".."
    os.chdir(path)
else :
    pass
MPI.barrier(comm)

tempo_fin=time.time()
if rank == 0 :
    print "DURATION cycle_diel.py=", tempo_fin-tempo_in
    print "END cycle_diel.py\n"