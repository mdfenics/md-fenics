// This code was created by PyGmsh v3.0.2.
p77 = newp;
Point(p77) = {4e-05, 4.5e-05, 4.8e-05, 9.149999999999999e-07};
p78 = newp;
Point(p78) = {4.61e-05, 4.5e-05, 4.8e-05, 9.149999999999999e-07};
p79 = newp;
Point(p79) = {4e-05, 5.11e-05, 4.8e-05, 9.149999999999999e-07};
p80 = newp;
Point(p80) = {4e-05, 4.5e-05, 5.41e-05, 9.149999999999999e-07};
p81 = newp;
Point(p81) = {3.3900000000000004e-05, 4.5e-05, 4.8e-05, 9.149999999999999e-07};
p82 = newp;
Point(p82) = {4e-05, 3.8900000000000004e-05, 4.8e-05, 9.149999999999999e-07};
p83 = newp;
Point(p83) = {4e-05, 4.5e-05, 4.19e-05, 9.149999999999999e-07};
l132 = newl;
Ellipse(l132) = {p78, p77, p83, p83};
l133 = newl;
Ellipse(l133) = {p83, p77, p81, p81};
l134 = newl;
Ellipse(l134) = {p81, p77, p80, p80};
l135 = newl;
Ellipse(l135) = {p80, p77, p78, p78};
l136 = newl;
Ellipse(l136) = {p78, p77, p79, p79};
l137 = newl;
Ellipse(l137) = {p79, p77, p81, p81};
l138 = newl;
Ellipse(l138) = {p81, p77, p82, p82};
l139 = newl;
Ellipse(l139) = {p82, p77, p78, p78};
l140 = newl;
Ellipse(l140) = {p83, p77, p79, p79};
l141 = newl;
Ellipse(l141) = {p79, p77, p80, p80};
l142 = newl;
Ellipse(l142) = {p80, p77, p82, p82};
l143 = newl;
Ellipse(l143) = {p82, p77, p83, p83};
ll88 = newll;
Line Loop(ll88) = {l136, l141, l135};
ll89 = newll;
Line Loop(ll89) = {l140, -l136, l132};
ll90 = newll;
Line Loop(ll90) = {-l141, l137, l134};
ll91 = newll;
Line Loop(ll91) = {-l137, -l140, l133};
ll92 = newll;
Line Loop(ll92) = {l139, -l135, l142};
ll93 = newll;
Line Loop(ll93) = {l143, -l139, -l132};
ll94 = newll;
Line Loop(ll94) = {-l142, -l134, l138};
ll95 = newll;
Line Loop(ll95) = {-l133, -l138, -l143};
rs88 = news;
Ruled Surface(rs88) = {ll88};
rs89 = news;
Ruled Surface(rs89) = {ll89};
rs90 = news;
Ruled Surface(rs90) = {ll90};
rs91 = news;
Ruled Surface(rs91) = {ll91};
rs92 = news;
Ruled Surface(rs92) = {ll92};
rs93 = news;
Ruled Surface(rs93) = {ll93};
rs94 = news;
Ruled Surface(rs94) = {ll94};
rs95 = news;
Ruled Surface(rs95) = {ll95};
s22 = news;
Compound Surface(s22) = {rs88,rs89,rs90,rs91};
s23 = news;
Compound Surface(s23) = {rs92,rs93,rs94,rs95};
sl11 = news;
Surface Loop(sl11) = {s22,s23};
Physical Surface("SphereSurf0") = s22;
Physical Surface("SphereSurf1") = s23;