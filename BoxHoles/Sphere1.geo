// This code was created by PyGmsh v3.0.2.
p7 = newp;
Point(p7) = {0.000187, 1.05e-05, 5e-05, 9.149999999999999e-07};
p8 = newp;
Point(p8) = {0.0001931, 1.05e-05, 5e-05, 9.149999999999999e-07};
p9 = newp;
Point(p9) = {0.000187, 1.66e-05, 5e-05, 9.149999999999999e-07};
p10 = newp;
Point(p10) = {0.000187, 1.05e-05, 5.61e-05, 9.149999999999999e-07};
p11 = newp;
Point(p11) = {0.00018089999999999998, 1.05e-05, 5e-05, 9.149999999999999e-07};
p12 = newp;
Point(p12) = {0.000187, 4.399999999999999e-06, 5e-05, 9.149999999999999e-07};
p13 = newp;
Point(p13) = {0.000187, 1.05e-05, 4.39e-05, 9.149999999999999e-07};
l12 = newl;
Ellipse(l12) = {p8, p7, p13, p13};
l13 = newl;
Ellipse(l13) = {p13, p7, p11, p11};
l14 = newl;
Ellipse(l14) = {p11, p7, p10, p10};
l15 = newl;
Ellipse(l15) = {p10, p7, p8, p8};
l16 = newl;
Ellipse(l16) = {p8, p7, p9, p9};
l17 = newl;
Ellipse(l17) = {p9, p7, p11, p11};
l18 = newl;
Ellipse(l18) = {p11, p7, p12, p12};
l19 = newl;
Ellipse(l19) = {p12, p7, p8, p8};
l20 = newl;
Ellipse(l20) = {p13, p7, p9, p9};
l21 = newl;
Ellipse(l21) = {p9, p7, p10, p10};
l22 = newl;
Ellipse(l22) = {p10, p7, p12, p12};
l23 = newl;
Ellipse(l23) = {p12, p7, p13, p13};
ll8 = newll;
Line Loop(ll8) = {l16, l21, l15};
ll9 = newll;
Line Loop(ll9) = {l20, -l16, l12};
ll10 = newll;
Line Loop(ll10) = {-l21, l17, l14};
ll11 = newll;
Line Loop(ll11) = {-l17, -l20, l13};
ll12 = newll;
Line Loop(ll12) = {l19, -l15, l22};
ll13 = newll;
Line Loop(ll13) = {l23, -l19, -l12};
ll14 = newll;
Line Loop(ll14) = {-l22, -l14, l18};
ll15 = newll;
Line Loop(ll15) = {-l13, -l18, -l23};
rs8 = news;
Ruled Surface(rs8) = {ll8};
rs9 = news;
Ruled Surface(rs9) = {ll9};
rs10 = news;
Ruled Surface(rs10) = {ll10};
rs11 = news;
Ruled Surface(rs11) = {ll11};
rs12 = news;
Ruled Surface(rs12) = {ll12};
rs13 = news;
Ruled Surface(rs13) = {ll13};
rs14 = news;
Ruled Surface(rs14) = {ll14};
rs15 = news;
Ruled Surface(rs15) = {ll15};
s2 = news;
Compound Surface(s2) = {rs8,rs9,rs10,rs11};
s3 = news;
Compound Surface(s3) = {rs12,rs13,rs14,rs15};
sl1 = news;
Surface Loop(sl1) = {s2,s3};
Physical Surface("SphereSurf0") = s2;
Physical Surface("SphereSurf1") = s3;